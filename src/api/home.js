import request from '@/utils/request'

// 站点首页区域信息
export const siteIndexAdDivision = (data) => {
  return request({
    url: '/site/index/adDivision',
    method: 'post',
    data: data
  })
}

// 站点首页基础信息
export const siteIndexBase = (data) => {
  return request({
    url: '/site/index/base',
    method: 'post',
    data: data
  })
}

// 站点首页报文信息
export const siteIndexDbRate = (data) => {
  return request({
    url: '/site/index/dbRate',
    method: 'post',
    data: data
  })
}

// 站点首页在线排名信息
export const siteIndexOnline = (data) => {
  return request({
    url: '/site/index/online',
    method: 'post',
    data: data
  })
}

// 站点首页状态信息
export const siteIndexStatus = (data) => {
  return request({
    url: '/site/index/status',
    method: 'post',
    data: data
  })
}

// 站点首页故障信息
export const siteIndexTrouble = (data) => {
  return request({
    url: '/site/index/trouble',
    method: 'post',
    data: data
  })
}

// 站点首页质报信息
export const siteIndexWarranty = (data) => {
  return request({
    url: '/site/index/warranty',
    method: 'post',
    data: data
  })
}
