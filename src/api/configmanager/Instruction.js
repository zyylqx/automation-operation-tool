import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 分页查询
export function instructionPage(data) {
  return request({
    url: '/instruction/page',
    method: 'post',
    data: data
  })
}
// 获取指令详情
export function instructionDetail(id) {
  return request({
    url: '/instruction/detail/' + parseStrEmpty(id),
    method: 'get'
  })
}
// 新增指令
export function instructionCreate(data) {
  return request({
    url: '/instruction/create',
    method: 'post',
    data: data
  })
}

// 删除指令
export function instructionDelete(id) {
  return request({
    url: '/instruction/delete/' + id,
    method: 'delete'
  })
}

// 更新指令
export function instructionUpdate(data) {
  return request({
    url: '/instruction/update',
    method: 'post',
    data: data
  })
}

// 取消绑定
export function instructionUnbindSite(data) {
  return request({
    url: '/instruction/unbindSite',
    method: 'post',
    data: data
  })
}

// 绑定站点
export function instructionBindSite(data) {
  return request({
    url: '/instruction/bindSite',
    method: 'post',
    data: data
  })
}
