import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 根据类型ID获取常量
export function getCategoryId(categoryId) {
  return request({
    url: '/system/constant/getCategoryId?categoryId=' + categoryId,
    method: 'get',
  })
}

// 根据类型key获取常量
export function getCategoryKey(categoryKey) {
  return request({
    url: '/system/constant/getCategoryKey?categoryKey=' + categoryKey,
    method: 'get',
  })
}

// 根据类型key获取常量
export function getCategoryKeyAndType(categoryKey, type) {
  return request({
    url: '/system/constant/getCategoryKeyAndType?categoryKey=' + categoryKey + '&type=' + type,
    method: 'get',
  })
}

// 根据上级ID获取常量
export function getParentId(parentId) {
  return request({
    url: '/system/constant/getParentId?parentId=' + parentId,
    method: 'get',
  })
}

// 获取所有常量
export function getParentIndex() {
  return request({
    url: '/system/constant/index',
    method: 'get',
  })
}



// 获取常量分页列表
export function itemListPage(data) {
  return request({
    url: '/system/constant/itemList',
    method: 'post',
    data: data
  })
}

// 获取常量详情
export function getItem(id) {
  return request({
    url: '/system/constant/getItem/' + id,
    method: 'get',
  })
}

// 新增常量
export function itemSave(data) {
  return request({
    url: '/system/constant/saveItem',
    method: 'post',
    data: data
  })
}

// 更新常量
export function itemUpdate(data) {
  return request({
    url: '/system/constant/updateItem',
    method: 'post',
    data: data
  })
}
