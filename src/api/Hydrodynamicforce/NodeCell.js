import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 水动力-节点网格关联关系表 : Node Cell Config ControllerShow/HideList OperationsExpand Operations
// post /mtool/sdl/nodeCellConfig/create保存
// post /mtool/sdl/nodeCellConfig/delete删除
// post /mtool/sdl/nodeCellConfig/download下载模板
// post /mtool/sdl/nodeCellConfig/export导出
// post /mtool/sdl/nodeCellConfig/import导入水动力模型配置
// post /mtool/sdl/nodeCellConfig/page分页查询
// 分页查询
export function mtoolsdlnodeCellConfigPage(data) {
  return request({
    url: '/mtool/sdl/nodeCellConfig/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolsdlnodeCellConfigCreate(data) {
  return request({
    url: '/mtool/sdl/nodeCellConfig/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolsdlnodeCellConfigUpdate(data) {
  return request({
    url: '/mtool/sdl/nodeCellConfig/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolsdlnodeCellConfigDelete(data) {
  return request({
    url: '/mtool/sdl/nodeCellConfig/delete',
    method: 'post',
    data: data
  })
}

// 下载模板
export function mtoolsdlnodeCellConfigDownload() {
  return request({
    url: '/mtool/sdl/nodeCellConfig/download',
    method: 'get',
  })
}
// 导出
export function mtoolsdlnodeCellConfigExport(data) {
  return request({
    url: '/mtool/sdl/nodeCellConfig/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolsdlnodeCellConfigImport(data) {
  return request({
    url: '/mtool/sdl/nodeCellConfig/import',
    method: 'post',
    data: data
  })
}
