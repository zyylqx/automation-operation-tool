import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 水动力-网格关系 : Sys Cell ControllerShow/HideList OperationsExpand Operations
// post /mtool/sdl/sysCell/create新增
// post /mtool/sdl/sysCell/delete删除
// get /mtool/sdl/sysCell/detail/{id}查询详情(无主键)
// post /mtool/sdl/sysCell/download下载模板
// post /mtool/sdl/sysCell/export导出
// post /mtool/sdl/sysCell/import导入水动力模型配置
// post /mtool/sdl/sysCell/page分页查询
// post /mtool/sdl/sysCell/update更新(无主键无法更新
// 分页查询
export function mtoolsdlsysCellPage(data) {
  return request({
    url: '/mtool/sdl/sysCell/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolsdlsysCellCreate(data) {
  return request({
    url: '/mtool/sdl/sysCell/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolsdlsysCellUpdate(data) {
  return request({
    url: '/mtool/sdl/sysCell/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolsdlsysCellDelete(data) {
  return request({
    url: '/mtool/sdl/sysCell/delete',
    method: 'post',
    data: data
  })
}

// 下载模板
export function mtoolsdlsysCellDownload() {
  return request({
    url: '/mtool/sdl/sysCell/download',
    method: 'get',
  })
}
// 导出
export function mtoolsdlsysCellExport(data) {
  return request({
    url: '/mtool/sdl/sysCell/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolsdlsysCellImport(data) {
  return request({
    url: '/mtool/sdl/sysCell/import',
    method: 'post',
    data: data
  })
}
