import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 水动力边界关系 : Hydrology Runner Config ControllerShow/HideList OperationsExpand Operations
// post /mtool/sdl/hydrologyRunnerConfig/create保存
// post /mtool/sdl/hydrologyRunnerConfig/delete删除
// post /mtool/sdl/hydrologyRunnerConfig/download下载模板
// post /mtool/sdl/hydrologyRunnerConfig/export导出
// post /mtool/sdl/hydrologyRunnerConfig/import导入水动力模型配置
// post /mtool/sdl/hydrologyRunnerConfig/page分页查询
// post /mtool/sdl/hydrologyRunnerConfig/update更新(无主键无法更新)

// 分页查询
export function mtoolsdlhydrologyRunnerConfigPage(data) {
  return request({
    url: '/mtool/sdl/hydrologyRunnerConfig/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolsdlhydrologyRunnerConfigCreate(data) {
  return request({
    url: '/mtool/sdl/hydrologyRunnerConfig/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolsdlhydrologyRunnerConfigUpdate(data) {
  return request({
    url: '/mtool/sdl/hydrologyRunnerConfig/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolsdlhydrologyRunnerConfigDelete(data) {
  return request({
    url: '/mtool/sdl/hydrologyRunnerConfig/delete',
    method: 'post',
    data: data
  })
}

// 下载模板
export function mtoolsdlhydrologyRunnerConfigDownload() {
  return request({
    url: '/mtool/sdl/hydrologyRunnerConfig/download',
    method: 'get',
  })
}
// 导出
export function mtoolsdlhydrologyRunnerConfigExport(data) {
  return request({
    url: '/mtool/sdl/hydrologyRunnerConfig/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolsdlhydrologyRunnerConfigImport(data) {
  return request({
    url: '/mtool/sdl/hydrologyRunnerConfig/import',
    method: 'post',
    data: data
  })
}
