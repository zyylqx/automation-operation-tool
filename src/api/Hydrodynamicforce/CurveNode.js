import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 水动力-断面节点关联关系 : Curve Node Config ControllerShow/HideList OperationsExpand Operations
// post /mtoolservice/sdl/curveNodeConfig/create保存
// post /mtoolservice/sdl/curveNodeConfig/delete删除
// post /mtoolservice/sdl/curveNodeConfig/download下载模板
// post /mtoolservice/sdl/curveNodeConfig/export导出
// post /mtoolservice/sdl/curveNodeConfig/import导入水动力模型配置
// post /mtoolservice/sdl/curveNodeConfig/page分页查询
// 分页查询
export function mtoolsdlcurveNodeConfigPage(data) {
  return request({
    url: '/mtool/sdl/curveNodeConfig/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolsdlcurveNodeConfigCreate(data) {
  return request({
    url: '/mtool/sdl/curveNodeConfig/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolsdlcurveNodeConfigUpdate(data) {
  return request({
    url: '/mtool/sdl/curveNodeConfig/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolsdlcurveNodeConfigDelete(data) {
  return request({
    url: '/mtool/sdl/curveNodeConfig/delete',
    method: 'post',
    data: data
  })
}

// 下载模板
export function mtoolsdlcurveNodeConfigDownload() {
  return request({
    url: '/mtool/sdl/curveNodeConfig/download',
    method: 'get',
  })
}
// 导出
export function mtoolsdlcurveNodeConfigExport(data) {
  return request({
    url: '/mtool/sdl/curveNodeConfig/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolsdlcurveNodeConfigImport(data) {
  return request({
    url: '/mtool/sdl/curveNodeConfig/import',
    method: 'post',
    data: data
  })
}
