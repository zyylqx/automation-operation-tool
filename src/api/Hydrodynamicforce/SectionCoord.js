import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 水动力-断面数据管理 : Section Coord ControllerShow/HideList OperationsExpand Operations
// post /mtool/sdl/sectionCoord/create保存
// post /mtool/sdl/sectionCoord/delete删除
// post /mtool/sdl/sectionCoord/download下载模板
// post /mtool/sdl/sectionCoord/export导出
// post /mtool/sdl/sectionCoord/import导入水动力模型配置
// post /mtool/sdl/sectionCoord/page分页查询
// post /mtool/sdl/sectionCoord/update更新(无主键无法更新)
// 分页查询
export function mtoolsdlsectionCoordPage(data) {
  return request({
    url: '/mtool/sdl/sectionCoord/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolsdlsectionCoordCreate(data) {
  return request({
    url: '/mtool/sdl/sectionCoord/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolsdlsectionCoordUpdate(data) {
  return request({
    url: '/mtool/sdl/sectionCoord/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolsdlsectionCoordDelete(data) {
  return request({
    url: '/mtool/sdl/sectionCoord/delete',
    method: 'post',
    data: data
  })
}

// 下载模板
export function mtoolsdlsectionCoordDownload() {
  return request({
    url: '/mtool/sdl/sectionCoord/download',
    method: 'get',
  })
}
// 导出
export function mtoolsdlsectionCoordExport(data) {
  return request({
    url: '/mtool/sdl/sectionCoord/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolsdlsectionCoordImport(data) {
  return request({
    url: '/mtool/sdl/sectionCoord/import',
    method: 'post',
    data: data
  })
}
