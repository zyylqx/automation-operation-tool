import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 水动力-大流域断面关联关系 : Sys Section ControllerShow/HideList OperationsExpand Operations
// post /mtool/sdl/sysSection/create保存
// post /mtool/sdl/sysSection/delete删除
// post /mtool/sdl/sysSection/download下载模板
// post /mtool/sdl/sysSection/export导出
// post /mtool/sdl/sysSection/import导入水动力模型配置
// post /mtool/sdl/sysSection/page分页查询
// post /mtool/sdl/sysSection/update更新(无主键无法更新)
// 分页查询
export function mtoolsdlsysSectionPage(data) {
  return request({
    url: '/mtool/sdl/sysSection/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolsdlsysSectionCreate(data) {
  return request({
    url: '/mtool/sdl/sysSection/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolsdlsysSectionUpdate(data) {
  return request({
    url: '/mtool/sdl/sysSection/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolsdlsysSectionDelete(data) {
  return request({
    url: '/mtool/sdl/sysSection/delete',
    method: 'post',
    data: data
  })
}

// 下载模板
export function mtoolsdlsysSectionDownload() {
  return request({
    url: '/mtool/sdl/sysSection/download',
    method: 'get',
  })
}
// 导出
export function mtoolsdlsysSectionExport(data) {
  return request({
    url: '/mtool/sdl/sysSection/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolsdlsysSectionImport(data) {
  return request({
    url: '/mtool/sdl/sysSection/import',
    method: 'post',
    data: data
  })
}
