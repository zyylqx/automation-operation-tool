import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 水动力-风险点 : Danger Points ControllerShow/HideList OperationsExpand Operations
// post /mtool/sdl/dangerPoints/create保存
// delete /mtool/sdl/dangerPoints/delete/{id}删除
// get /mtool/sdl/dangerPoints/detail/{id}查询详情
// post /mtool/sdl/dangerPoints/download下载模板
// post /mtool/sdl/dangerPoints/export导出
// post /mtool/sdl/dangerPoints/import导入风险点
// post /mtool/sdl/dangerPoints/page分页查询
// post /mtool/sdl/dangerPoints/update更新
// 分页查询
export function mtoolsdldangerPointsPage(data) {
  return request({
    url: '/mtool/sdl/dangerPoints/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolsdldangerPointsCreate(data) {
  return request({
    url: '/mtool/sdl/dangerPoints/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolsdldangerPointsUpdate(data) {
  return request({
    url: '/mtool/sdl/dangerPoints/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolsdldangerPointsDelete(id) {
  return request({
    url: '/mtool/sdl/dangerPoints/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtoolsdldangerPointsDownload() {
  return request({
    url: '/mtool/sdl/dangerPoints/download',
    method: 'get',
  })
}
// 导出
export function mtoolsdldangerPointsExport(data) {
  return request({
    url: '/mtool/sdl/dangerPoints/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolsdldangerPointsImport(data) {
  return request({
    url: '/mtool/sdl/dangerPoints/import',
    method: 'post',
    data: data
  })
}
