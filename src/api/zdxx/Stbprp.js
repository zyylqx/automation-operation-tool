import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 站点信息-测站基本属性表 : St Stbprp B ControllerShow/HideList OperationsExpand Operations
// post /mtool/st/stbprp/create新增测站基本属性信息
// delete /mtool/st/stbprp/delete/{stcd}删除测站基本属性信息
// post /mtool/st/stbprp/download下载模板
// post /mtool/st/stbprp/export导出
// post /mtool/st/stbprp/import导入
// post /mtool/st/stbprp/page分页查询
// post /mtool/st/stbprp/update更新测站基本属性信息

// 分页查询
export function mtoolststbprpPage(data) {
  return request({
    url: '/mtool/st/stbprp/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolststbprpCreate(data) {
  return request({
    url: '/mtool/st/stbprp/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolststbprpUpdate(data) {
  return request({
    url: '/mtool/st/stbprp/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolststbprpDelete(data) {
  return request({
    url: '/mtool/st/stbprp/delete/'+data.stcd,
    method: 'delete',
  })
}

// 下载模板
export function mtoolststbprpDownload() {
  return request({
    url: '/mtool/st/stbprp/download',
    method: 'get',
  })
}
// 导出
export function mtoolststbprpExport(data) {
  return request({
    url: '/mtool/st/stbprp/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolststbprpImport(data) {
  return request({
    url: '/mtool/st/stbprp/import',
    method: 'post',
    data: data
  })
}
