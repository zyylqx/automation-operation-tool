import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 站点信息-小流域站点关联关系表 : St Wata Weight ControllerShow/HideList OperationsExpand Operations
// post /mtool/st/wata/weight/create
// 新增小流域站点关联关系信息
// delete /mtool/st/wata/weight/delete
// 删除小流域站点关联关系信息
// post /mtool/st/wata/weight/download
// 下载模板
// post /mtool/st/wata/weight/export
// 导出
// post /mtool/st/wata/weight/import
// 导入
// post /mtool/st/wata/weight/page
// 分页查询
// post /mtool/st/wata/weight/update
// 更新小流域站点关联关系信息

// 分页查询
export function mtoolstwataweightPage(data) {
  return request({
    url: '/mtool/st/wata/weight/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolstwataweightCreate(data) {
  return request({
    url: '/mtool/st/wata/weight/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolstwataweightUpdate(data) {
  return request({
    url: '/mtool/st/wata/weight/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolstwataweightDelete(id) {
  return request({
    url: '/mtool/st/wata/weight/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtoolstwataweightDownload() {
  return request({
    url: '/mtool/st/wata/weight/download',
    method: 'get',
  })
}
// 导出
export function mtoolstwataweightExport(data) {
  return request({
    url: '/mtool/st/wata/weight/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolstwataweightImport(data) {
  return request({
    url: '/mtool/st/wata/weight/import',
    method: 'post',
    data: data
  })
}
