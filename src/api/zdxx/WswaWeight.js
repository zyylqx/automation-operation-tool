import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 站点信息-大流域站点关联关系表 : St Wswa Weight ControllerShow/HideList OperationsExpand Operations
// post /mtool/st/wswa/weight/create新增大流域站点关联关系信息
// delete /mtool/st/wswa/weight/delete删除大流域站点关联关系信息
// post /mtool/st/wswa/weight/download下载模板
// post /mtool/st/wswa/weight/export导出
// post /mtool/st/wswa/weight/import导入
// post /mtool/st/wswa/weight/page分页查询
// post /mtool/st/wswa/weight/update更新大流域站点关联关系信息

// 分页查询
export function mtoolstwswaweightPage(data) {
  return request({
    url: '/mtool/st/wswa/weight/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolstwswaweightCreate(data) {
  return request({
    url: '/mtool/st/wswa/weight/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolstwswaweightUpdate(data) {
  return request({
    url: '/mtool/st/wswa/weight/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolstwswaweightDelete(data) {
  return request({
    url: '/mtool/st/wswa/weight/delete',
    method: 'post',
    data: data
  })
}

// 下载模板
export function mtoolstwswaweightDownload() {
  return request({
    url: '/mtool/st/wswa/weight/download',
    method: 'get',
  })
}
// 导出
export function mtoolstwswaweightExport(data) {
  return request({
    url: '/mtool/st/wswa/weight/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolstwswaweightImport(data) {
  return request({
    url: '/mtool/st/wswa/weight/import',
    method: 'post',
    data: data
  })
}
