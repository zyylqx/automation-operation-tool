import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 站点信息-村庄站点关联关系表 : St Village Config ControllerShow/HideList OperationsExpand Operations
// post /mtool/st/village/config/create
// 新增村庄站点关联关系信息
// delete /mtool/st/village/config/delete
// 删除村庄站点关联关系信息
// post /mtool/st/village/config/download
// 下载模板
// post /mtool/st/village/config/export
// 导出
// post /mtool/st/village/config/import
// 导入
// post /mtool/st/village/config/page
// 分页查询
// post /mtool/st/village/config/update
// 更新村庄站点关联关系信息

// 分页查询
export function mtoolstvillageconfigPage(data) {
  return request({
    url: '/mtool/st/village/config/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolstvillageconfigCreate(data) {
  return request({
    url: '/mtool/st/village/config/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolstvillageconfigUpdate(data) {
  return request({
    url: '/mtool/st/village/config/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolstvillageconfigDelete(data) {
  return request({
    url: '/mtool/st/village/config/delete',
    method: 'post',
    data: data
  })
}

// 下载模板
export function mtoolstvillageconfigDownload() {
  return request({
    url: '/mtool/st/village/config/download',
    method: 'get',
  })
}
// 导出
export function mtoolstvillageconfigExport(data) {
  return request({
    url: '/mtool/st/village/config/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolstvillageconfigImport(data) {
  return request({
    url: '/mtool/st/village/config/import',
    method: 'post',
    data: data
  })
}
