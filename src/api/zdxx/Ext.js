import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 站点信息-测站扩展表 : St Ext ControllerShow/HideList OperationsExpand Operations
// post /mtool/st/ext/create新增测站扩展信息
// delete /mtool/st/ext/delete/{stcd}删除测站扩展信息
// post /mtool/st/ext/download下载模板
// post /mtool/st/ext/export
// 导出
// post /mtool/st/ext/import
// 导入
// post /mtool/st/ext/page
// 分页查询
// post /mtool/st/ext/update
// 更新测站扩展信息

// 分页查询
export function mtoolstextPage(data) {
  return request({
    url: '/mtool/st/ext/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolstextCreate(data) {
  return request({
    url: '/mtool/st/ext/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolstextUpdate(data) {
  return request({
    url: '/mtool/st/ext/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolstextDelete(data) {
  return request({
    url: '/mtool/st/ext/delete/'+data.stcd,
    method: 'delete',
    data: data
  })
}

// 下载模板
export function mtoolstextDownload() {
  return request({
    url: '/mtool/st/ext/download',
    method: 'get',
  })
}
// 导出
export function mtoolstextExport(data) {
  return request({
    url: '/mtool/st/ext/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolstextImport(data) {
  return request({
    url: '/mtool/st/ext/import',
    method: 'post',
    data: data
  })
}
