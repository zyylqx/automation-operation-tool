import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 指标-流域降雨频率指标 : Rain Design Ws ControllerShow/HideList OperationsExpand Operations
// post /mtool/zb/rainDesignWs/create保存
// post /mtool/zb/rainDesignWs/delete删除
// get /mtool/zb/rainDesignWs/detail/{id}查询详情
// post /mtool/zb/rainDesignWs/download下载模板
// post /mtool/zb/rainDesignWs/export导出
// post /mtool/zb/rainDesignWs/import导入
// post /mtool/zb/rainDesignWs/pageList分页查询
// post /mtool/zb/rainDesignWs/update更新

// 分页查询
export function mtoolzbrainDesignWsPage(data) {
  return request({
    url: '/mtool/zb/rainDesignWs/pageList',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolzbrainDesignWsCreate(data) {
  return request({
    url: '/mtool/zb/rainDesignWs/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolzbrainDesignWsUpdate(data) {
  return request({
    url: '/mtool/zb/rainDesignWs/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolzbrainDesignWsDelete(data) {
  return request({
    url: '/mtool/zb/rainDesignWs/delete',
    method: 'post',
    data: data
  })
}

// 下载模板
export function mtoolzbrainDesignWsDownload() {
  return request({
    url: '/mtool/zb/rainDesignWs/download',
    method: 'get',
  })
}
// 导出
export function mtoolzbrainDesignWsExport(data) {
  return request({
    url: '/mtool/zb/rainDesignWs/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolzbrainDesignWsImport(data) {
  return request({
    url: '/mtool/zb/rainDesignWs/import',
    method: 'post',
    data: data
  })
}
