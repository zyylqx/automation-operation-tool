import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 指标-河道预警指标表 : Index River ControllerShow/HideList OperationsExpand Operations
// post /mtool/zb/indexRiver/create保存
// delete /mtool/zb/indexRiver/delete/{rvcd}删除
// get /mtool/zb/indexRiver/detail/{rvcd}查询详情
// post /mtool/zb/indexRiver/download下载模板
// post /mtool/zb/indexRiver/export导出
// post /mtool/zb/indexRiver/import导入
// post /mtool/zb/indexRiver/pageList分页查询
// post /mtool/zb/indexRiver/update更新

// 分页查询
export function mtoolzbindexRiverPage(data) {
  return request({
    url: '/mtool/zb/indexRiver/pageList',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolzbindexRiverCreate(data) {
  return request({
    url: '/mtool/zb/indexRiver/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolzbindexRiverUpdate(data) {
  return request({
    url: '/mtool/zb/indexRiver/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolzbindexRiverDelete(data) {
  return request({
    url: '/mtool/zb/indexRiver/delete/' + data,
    method: 'delete',
    data: data
  })
}

// 下载模板
export function mtoolzbindexRiverDownload() {
  return request({
    url: '/mtool/zb/indexRiver/download',
    method: 'get',
  })
}
// 导出
export function mtoolzbindexRiverExport(data) {
  return request({
    url: '/mtool/zb/indexRiver/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolzbindexRiverImport(data) {
  return request({
    url: '/mtool/zb/indexRiver/import',
    method: 'post',
    data: data
  })
}
