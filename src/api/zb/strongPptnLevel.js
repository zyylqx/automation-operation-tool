import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 指标-降水级别定义 : Strong Pptn Level ControllerShow/HideList OperationsExpand Operations
// post /mtool/zb/strongPptnLevel/create保存
// delete /mtool/zb/strongPptnLevel/delete/{id}删除
// get /mtool/zb/strongPptnLevel/detail/{id}查询详情
// post /mtool/zb/strongPptnLevel/download下载模板
// post /mtool/zb/strongPptnLevel/export导出
// post /mtool/zb/strongPptnLevel/import导入
// post /mtool/zb/strongPptnLevel/pageList分页查询
// post /mtool/zb/strongPptnLevel/update保存
// 分页查询
export function mtoolzbstrongPptnLevelPage(data) {
  return request({
    url: '/mtool/zb/strongPptnLevel/pageList',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolzbstrongPptnLevelCreate(data) {
  return request({
    url: '/mtool/zb/strongPptnLevel/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolzbstrongPptnLevelUpdate(data) {
  return request({
    url: '/mtool/zb/strongPptnLevel/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolzbstrongPptnLevelDelete(data) {
  return request({
    url: '/mtool/zb/strongPptnLevel/delete/' + data,
    method: 'delete',
   
  })
}

// 下载模板
export function mtoolzbstrongPptnLevelDownload() {
  return request({
    url: '/mtool/zb/strongPptnLevel/download',
    method: 'get',
  })
}
// 导出
export function mtoolzbstrongPptnLevelExport(data) {
  return request({
    url: '/mtool/zb/strongPptnLevel/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolzbstrongPptnLevelImport(data) {
  return request({
    url: '/mtool/zb/strongPptnLevel/import',
    method: 'post',
    data: data
  })
}
