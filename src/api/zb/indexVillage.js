import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 指标-村庄预警指标表 : Index Village ControllerShow/HideList OperationsExpand Operations
// post /mtool/zb/indexVillage/create
// 保存
// post /mtool/zb/indexVillage/delete
// 删除
// get /mtool/zb/indexVillage/detail/{id}
// 查询详情
// post /mtool/zb/indexVillage/download
// 下载模板
// post /mtool/zb/indexVillage/export
// 导出
// post /mtool/zb/indexVillage/import
// 导入
// post /mtool/zb/indexVillage/pageList
// 分页查询
// post /mtool/zb/indexVillage/update
// 更新

// 分页查询
export function mtoolzbindexVillagePage(data) {
  return request({
    url: '/mtool/zb/indexVillage/pageList',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolzbindexVillageCreate(data) {
  return request({
    url: '/mtool/zb/indexVillage/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolzbindexVillageUpdate(data) {
  return request({
    url: '/mtool/zb/indexVillage/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolzbindexVillageDelete(data) {
  return request({
    url: '/mtool/zb/indexVillage/delete',
    method: 'post',
    data: data
  })
}

// 下载模板
export function mtoolzbindexVillageDownload() {
  return request({
    url: '/mtool/zb/indexVillage/download',
    method: 'get',
  })
}
// 导出
export function mtoolzbindexVillageExport(data) {
  return request({
    url: '/mtool/zb/indexVillage/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolzbindexVillageImport(data) {
  return request({
    url: '/mtool/zb/indexVillage/import',
    method: 'post',
    data: data
  })
}
