import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 指标-河道频率指标 : Rv Design Flood ControllerShow/HideList OperationsExpand Operations
// post /mtool/zb/rvDesignFlood/create
// 保存

// delete /mtool/zb/rvDesignFlood/delete/{rvcd}
// 删除

// get /mtool/zb/rvDesignFlood/detail/{id}
// 查询详情

// post /mtool/zb/rvDesignFlood/download
// 下载模板

// post /mtool/zb/rvDesignFlood/export
// 导出

// post /mtool/zb/rvDesignFlood/import
// 导入

// post /mtool/zb/rvDesignFlood/pageList
// 分页查询

// post /mtool/zb/rvDesignFlood/update
// 更新

// 分页查询
export function mtoolzbrvDesignFloodPage(data) {
  return request({
    url: '/mtool/zb/rvDesignFlood/pageList',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolzbrvDesignFloodCreate(data) {
  return request({
    url: '/mtool/zb/rvDesignFlood/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolzbrvDesignFloodUpdate(data) {
  return request({
    url: '/mtool/zb/rvDesignFlood/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolzbrvDesignFloodDelete(data) {
  return request({
    url: '/mtool/zb/rvDesignFlood/delete/' + data.rvcd,
    method: 'delete',
    
  })
}

// 下载模板
export function mtoolzbrvDesignFloodDownload() {
  return request({
    url: '/mtool/zb/rvDesignFlood/download',
    method: 'get',
  })
}
// 导出
export function mtoolzbrvDesignFloodExport(data) {
  return request({
    url: '/mtool/zb/rvDesignFlood/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolzbrvDesignFloodImport(data) {
  return request({
    url: '/mtool/zb/rvDesignFlood/import',
    method: 'post',
    data: data
  })
}
