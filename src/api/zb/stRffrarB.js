import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 指标-站点暴雨频率成果 : St Rffrar B ControllerShow/HideList OperationsExpand Operations
// post /mtool/zb/stRffrarB/create保存
// post /mtool/zb/stRffrarB/delete删除
// post /mtool/zb/stRffrarB/download下载模板
// post /mtool/zb/stRffrarB/export导出
// post /mtool/zb/stRffrarB/import导入
// post /mtool/zb/stRffrarB/pageList分页查询
// post /mtool/zb/stRffrarB/update更新

// 分页查询
export function mtoolzbstRffrarBPage(data) {
  return request({
    url: '/mtool/zb/stRffrarB/pageList',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolzbstRffrarBCreate(data) {
  return request({
    url: '/mtool/zb/stRffrarB/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolzbstRffrarBUpdate(data) {
  return request({
    url: '/mtool/zb/stRffrarB/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolzbstRffrarBDelete(data) {
  return request({
    url: '/mtool/zb/stRffrarB/delete',
    method: 'post',
    data: data
  })
}

// 下载模板
export function mtoolzbstRffrarBDownload() {
  return request({
    url: '/mtool/zb/stRffrarB/download',
    method: 'get',
  })
}
// 导出
export function mtoolzbstRffrarBExport(data) {
  return request({
    url: '/mtool/zb/stRffrarB/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolzbstRffrarBImport(data) {
  return request({
    url: '/mtool/zb/stRffrarB/import',
    method: 'post',
    data: data
  })
}
