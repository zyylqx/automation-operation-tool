import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 气象-村庄和网格的关系 : Village Grid Config ControllerShow/HideList OperationsExpand Operations
// post /mtoolservice/qx/villageGridConfig/create保存
// post /mtoolservice/qx/villageGridConfig/delete删除
// get /mtoolservice/qx/villageGridConfig/detail/{id}查询详情
// post /mtoolservice/qx/villageGridConfig/download下载模板
// post /mtoolservice/qx/villageGridConfig/export导出
// post /mtoolservice/qx/villageGridConfig/import导入
// post /mtoolservice/qx/villageGridConfig/pageList分页查询

// 分页查询
export function mtoolqxvillageGridConfigPage(data) {
  return request({
    url: '/mtool/qx/villageGridConfig/pageList',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolqxvillageGridConfigCreate(data) {
  return request({
    url: '/mtool/qx/villageGridConfig/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolqxvillageGridConfigUpdate(data) {
  return request({
    url: '/mtool/qx/villageGridConfig/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolqxvillageGridConfigDelete(data) {
  return request({
    url: '/mtool/qx/villageGridConfig/delete',
    method: 'post',
    data: data
  })
}

// 下载模板
export function mtoolqxvillageGridConfigDownload() {
  return request({
    url: '/mtool/qx/villageGridConfig/download',
    method: 'get',
  })
}
// 导出
export function mtoolqxvillageGridConfigExport(data) {
  return request({
    url: '/mtool/qx/villageGridConfig/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolserviceqxvillageGridConfigImport(data) {
  return request({
    url: '/mtool/qx/villageGridConfig/import',
    method: 'post',
    data: data
  })
}
