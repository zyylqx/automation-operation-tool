import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 气象-村庄和网格的关系 : Village Grid Config ControllerShow/HideList OperationsExpand Operations
// post /mtoolservice/qx/wataGridConfigShort/create保存
// post /mtoolservice/qx/wataGridConfigShort/delete删除
// get /mtoolservice/qx/wataGridConfigShort/detail/{id}查询详情
// post /mtoolservice/qx/wataGridConfigShort/download下载模板
// post /mtoolservice/qx/wataGridConfigShort/export导出
// post /mtoolservice/qx/wataGridConfigShort/import导入
// post /mtoolservice/qx/wataGridConfigShort/pageList分页查询

// 分页查询
export function mtoolqxwataGridConfigShortPage(data) {
  return request({
    url: '/mtool/qx/wataGridConfigShort/pageList',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolqxwataGridConfigShortCreate(data) {
  return request({
    url: '/mtool/qx/wataGridConfigShort/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolqxwataGridConfigShortUpdate(data) {
  return request({
    url: '/mtool/qx/wataGridConfigShort/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolqxwataGridConfigShortDelete(data) {
  return request({
    url: '/mtool/qx/wataGridConfigShort/delete',
    method: 'post',
    data: data
  })
}

// 下载模板
export function mtoolqxwataGridConfigShortDownload() {
  return request({
    url: '/mtool/qx/wataGridConfigShort/download',
    method: 'get',
  })
}
// 导出
export function mtoolqxwataGridConfigShortExport(data) {
  return request({
    url: '/mtool/qx/wataGridConfigShort/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolqxwataGridConfigShortImport(data) {
  return request({
    url: '/mtool/qx/wataGridConfigShort/import',
    method: 'post',
    data: data
  })
}
