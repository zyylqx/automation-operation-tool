import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 气象-村庄和网格的关系 : Village Grid Config ControllerShow/HideList OperationsExpand Operations
// post /mtoolservice/qx/wataGridConfigDO/create保存
// post /mtoolservice/qx/wataGridConfigDO/delete删除
// get /mtoolservice/qx/wataGridConfigDO/detail/{id}查询详情
// post /mtoolservice/qx/wataGridConfigDO/download下载模板
// post /mtoolservice/qx/wataGridConfigDO/export导出
// post /mtoolservice/qx/wataGridConfigDO/import导入
// post /mtoolservice/qx/wataGridConfigDO/pageList分页查询

// 分页查询
export function mtoolqxwataGridConfigDOPage(data) {
  return request({
    url: '/mtool/qx/wataGridConfigDO/pageList',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolqxwataGridConfigDOCreate(data) {
  return request({
    url: '/mtool/qx/wataGridConfigDO/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolqxwataGridConfigDOUpdate(data) {
  return request({
    url: '/mtool/qx/wataGridConfigDO/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolqxwataGridConfigDODelete(data) {
  return request({
    url: '/mtool/qx/wataGridConfigDO/delete',
    method: 'post',
    data: data
  })
}

// 下载模板
export function mtoolqxwataGridConfigDODownload() {
  return request({
    url: '/mtool/qx/wataGridConfigDO/download',
    method: 'get',
  })
}
// 导出
export function mtoolqxwataGridConfigDOExport(data) {
  return request({
    url: '/mtool/qx/wataGridConfigDO/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolqxwataGridConfigDOImport(data) {
  return request({
    url: '/mtool/qx/wataGridConfigDO/import',
    method: 'post',
    data: data
  })
}
