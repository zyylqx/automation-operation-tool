import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 气象-村庄和网格的关系 : Village Grid Config ControllerShow/HideList OperationsExpand Operations
// post /mtoolservice/qx/stGridConfigShort/create保存
// post /mtoolservice/qx/stGridConfigShort/delete删除
// get /mtoolservice/qx/stGridConfigShort/detail/{id}查询详情
// post /mtoolservice/qx/stGridConfigShort/download下载模板
// post /mtoolservice/qx/stGridConfigShort/export导出
// post /mtoolservice/qx/stGridConfigShort/import导入
// post /mtoolservice/qx/stGridConfigShort/pageList分页查询

// 分页查询
export function mtoolserviceqxstGridConfigShortPage(data) {
  return request({
    url: '/mtool/qx/stGridConfigShort/pageList',
    method: 'post',
    data: data
  })
}
// 新增mtoolserviceqxvillageGridConfigCreate
export function mtoolserviceqxstGridConfigShortCreate(data) {
  return request({
    url: '/mtool/qx/stGridConfigShort/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolserviceqxstGridConfigShortUpdate(data) {
  return request({
    url: '/mtool/qx/stGridConfigShort/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolserviceqxstGridConfigShortDelete(data) {
  return request({
    url: '/mtool/qx/stGridConfigShort/delete',
    method: 'post',
    data: data
  })
}

// 下载模板
export function mtoolserviceqxstGridConfigShortDownload() {
  return request({
    url: '/mtoolservice/qx/stGridConfigShort/download',
    method: 'get',
  })
}
// 导出
export function mtoolserviceqxstGridConfigShortExport(data) {
  return request({
    url: '/mtool/qx/stGridConfigShort/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolserviceqxstGridConfigShortImport(data) {
  return request({
    url: '/mtool/qx/stGridConfigShort/import',
    method: 'post',
    data: data
  })
}
