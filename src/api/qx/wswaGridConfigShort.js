import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 气象-村庄和网格的关系 : Village Grid Config ControllerShow/HideList OperationsExpand Operations
// post /mtoolservice/qx/wswaGridConfigShort/create保存
// post /mtoolservice/qx/wswaGridConfigShort/delete删除
// get /mtoolservice/qx/wswaGridConfigShort/detail/{id}查询详情
// post /mtoolservice/qx/wswaGridConfigShort/download下载模板
// post /mtoolservice/qx/wswaGridConfigShort/export导出
// post /mtoolservice/qx/wswaGridConfigShort/import导入
// post /mtoolservice/qx/wswaGridConfigShort/pageList分页查询

// 分页查询
export function mtoolqxwswaGridConfigShortPage(data) {
  return request({
    url: '/mtool/qx/wswaGridConfigShort/pageList',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolqxwswaGridConfigShortCreate(data) {
  return request({
    url: '/mtool/qx/wswaGridConfigShort/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolqxwswaGridConfigShortUpdate(data) {
  return request({
    url: '/mtool/qx/wswaGridConfigShort/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolqxwswaGridConfigShortDelete(data) {
  return request({
    url: '/mtool/qx/wswaGridConfigShort/delete',
    method: 'post',
    data: data
  })
}

// 下载模板
export function mtoolserviceqxwswaGridConfigShortDownload() {
  return request({
    url: '/mtool/qx/wswaGridConfigShort/download',
    method: 'get',
  })
}
// 导出
export function mtoolqxwswaGridConfigShortExport(data) {
  return request({
    url: '/mtool/qx/wswaGridConfigShort/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolqxwswaGridConfigShortImport(data) {
  return request({
    url: '/mtool/qx/wswaGridConfigShort/import',
    method: 'post',
    data: data
  })
}
