import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 气象-村庄和网格的关系 : Village Grid Config ControllerShow/HideList OperationsExpand Operations
// post /mtoolservice/qx/wswaGridConfig/create保存
// post /mtoolservice/qx/wswaGridConfig/delete删除
// get /mtoolservice/qx/wswaGridConfig/detail/{id}查询详情
// post /mtoolservice/qx/wswaGridConfig/download下载模板
// post /mtoolservice/qx/wswaGridConfig/export导出
// post /mtoolservice/qx/wswaGridConfig/import导入
// post /mtoolservice/qx/wswaGridConfig/pageList分页查询

// 分页查询
export function mtoolqxwswaGridConfigPage(data) {
  return request({
    url: '/mtool/qx/wswaGridConfig/pageList',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolqxwswaGridConfigCreate(data) {
  return request({
    url: '/mtool/qx/wswaGridConfig/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolqxwswaGridConfigUpdate(data) {
  return request({
    url: '/mtool/qx/wswaGridConfig/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolqxwswaGridConfigDelete(data) {
  return request({
    url: '/mtool/qx/wswaGridConfig/delete',
    method: 'post',
    data: data
  })
}

// 下载模板
export function mtoolqxwswaGridConfigDownload() {
  return request({
    url: '/mtool/qx/wswaGridConfig/download',
    method: 'get',
  })
}
// 导出
export function mtoolqxwswaGridConfigExport(data) {
  return request({
    url: '/mtool/qx/wswaGridConfig/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolqxwswaGridConfigImport(data) {
  return request({
    url: '/mtool/qx/wswaGridConfig/import',
    method: 'post',
    data: data
  })
}
