import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 获取厂商分页列表
export function siteManufacturerList(data) {
  return request({
    url: '/site/manufacturer/list',
    method: 'post',
    data: data
  })
}
// 获取厂商详情
export function getManufacturerDetails(id) {
  return request({
    url: '/site/manufacturer/get/' + parseStrEmpty(id),
    method: 'get'
  })
}
// 新增厂商
export function siteMnufacturerSave(data) {
  return request({
    url: '/site/manufacturer/save',
    method: 'post',
    data: data
  })
}

// 修改厂商
export function siteManufacturerUpdate(reqDTO) {
  return request({
    url: '/site/manufacturer/update',
    method: 'put',
    data: reqDTO
  })
}

// 删除厂商
export function siteManufacturerRemove(id) {
  return request({
    url: '/site/manufacturer/remove/' + id,
    method: 'delete'
  })
}

// 绑定站点
export function siteMnufacturerBindSite(data) {
  return request({
    url: '/site/manufacturer/bindSite',
    method: 'post',
    data: data
  })
}

// 取消绑定
export function siteMnufacturerUnbindSitee(data) {
  return request({
    url: '/site/manufacturer/unbindSite',
    method: 'post',
    data: data
  })
}
