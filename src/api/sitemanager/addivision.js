import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 根据代码获取
export function getByAddvCode(addvCode) {
  return request({
    url: '/addv/getByAddvCode?upCode=' + parseStrEmpty(addvCode),
    method: 'get'
  })
}
// 根据上级代码获取
export function getByUpCode(upCode) {
  return request({
    url: '/addv/getByUpCode?upCode=' + parseStrEmpty(upCode),
    method: 'get'
  })
}

// 行政区划树形结构
export function getaddvTree() {
  return request({
    url: '/addv/tree/',
    method: 'get'
  })
}
