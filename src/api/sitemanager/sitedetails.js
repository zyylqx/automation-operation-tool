import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 站点水雨水情统计
export function monitorElementStatic(data) {
  return request({
    url: '/site/monitorElementStatic',
    method: 'post',
    data: data
  })
}

// 站点河道水情统计
export function RiverelementStatic(data) {
  return request({
    url: '/site/river/elementStatic',
    method: 'post',
    data: data
  })
}

// 站点降水量统计
export function PptnementStatic(data) {
  return request({
    url: '/site/pptn/elementStatic',
    method: 'post',
    data: data
  })
}

//站点指令召唤
export function calledMeasurement(data) {
  return request({
    url: '/site/inslog/calledMeasurement',
    method: 'post',
    data: data
  })
}