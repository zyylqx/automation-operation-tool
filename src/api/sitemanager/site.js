import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 查询站点列表
export function sitePage(data) {
  return request({
    url: '/site/page',
    method: 'post',
    data: data
  })
}

// 站点详情
export function siteDetail(id) {
  return request({
    url: '/site/detail/' + parseStrEmpty(id),
    method: 'get'
  })
}

// 站点详情
export function siteDetailCode(code) {
  return request({
    url: '/site/detailCode/' + parseStrEmpty(code),
    method: 'get'
  })
}


// 新增站点
export function siteCreate(data) {
  return request({
    url: '/site/create',
    method: 'post',
    data: data
  })
}

//修改站点
export function siteUpdate(data) {
  return request({
    url: '/site/update',
    method: 'post',
    data: data
  })
}

// 删除站点
export function siteDelete(id) {
  return request({
    url: '/site/delete/' + id,
    method: 'delete'
  })
}

// 批量删除站点
export function siteBatchDelete(idList) {
  return request({
    url: '/site/batchDelete',
    method: 'delete',
    data: idList
  })
}


// 站点关闭
export function siteClose(idList) {
  return request({
    url: '/site/close',
    method: 'put',
    data: idList
  })
}


// 站点统计
export function siteStatistics(data) {
  return request({
    url: '/site/statistics',
    method: 'post',
    data: data
  })
}

// 未绑定站点分页查询
export function siteUnbindPage(data) {
  return request({
    url: '/site/unbind/page',
    method: 'post',
    data: data
  })
}



// 监测要素列表
export function siteElementListAll(siteCode) {
  return request({
    url: '/site/element/listAll?siteCode=' + siteCode,
    method: 'get'
  })
}

// 监测三要素列表
export function siteElementList(siteCode) {
  return request({
    url: '/site/element/list?siteCode=' + siteCode,
    method: 'get'
  })
}

// 站点新增监测要素
export function siteElementAdd(data) {
  return request({
    url: '/site/element/add',
    method: 'post',
    data: data
  })
}

// 删除监测要素
export function siteElementDelete(id) {
  return request({
    url: 'site/element/delete/' + id,
    method: 'delete'
  })
}

// 更新监测要素
export function siteElementUpdate(data) {
  return request({
    url: '/site/element/update',
    method: 'post',
    data: data
  })
}

// 站点转调试
export function siteDebug(data) {
  return request({
    url: '/site/debug',
    method: 'put',
    data: data
  })
}

// 站点试运行
export function siteTryRun(idList) {
  return request({
    url: '/site/tryRun',
    method: 'put',
    data: idList
  })
}

// 站点转正
export function siteRunning(idList) {
  return request({
    url: '/site/running',
    method: 'put',
    data: idList
  })
}


// 站点指令查询
export function siteInsListSite(id) {
  return request({
    url: '/site/ins/listSite?id=' + id,
    method: 'get'
  })
}
// 站点指令发送
export function siteInslogAdd(data) {
  return request({
    url: '/site/inslog/add',
    method: 'post',
    data: data
  })
}
// 站点指令日志分页查询
export function siteInslogPage(data) {
  return request({
    url: '/site/inslog/page',
    method: 'post',
    data: data
  })
}
