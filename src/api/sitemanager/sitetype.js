import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 站点类型 分页查询
export function SiteTypePage(data) {
  return request({
    url: '/site/type/page',
    method: 'post',
    data: data
  })
}

// 站点类型详情
export function SiteTypeDetail(id) {
  return request({
    url: '/site/type/detail/' + parseStrEmpty(id),
    method: 'get'
  })
}

// 新增站点类型
export function addSiteType(data) {
  return request({
    url: '/site/type/create',
    method: 'post',
    data: data
  })
}

// 修改站点类型
export function updateSiteType(data) {
  return request({
    url: '/site/type/update',
    method: 'post',
    data: data
  })
}

// 删除站点类型
export function delSiteType(id) {
  return request({
    url: '/site/type/delete/' + id,
    method: 'delete'
  })
}

// 绑定站点
export function siteTypeBindSite(data) {
  return request({
    url: '/site/type/bindSite',
    method: 'post',
    data: data
  })
}

// 取消绑定
export function siteTypeUnbindSite(data) {
  return request({
    url: '/site/type/unbindSite',
    method: 'post',
    data: data
  })
}
