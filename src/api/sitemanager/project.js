import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 获取项目管理分页列表
export function siteProjectList(data) {
  return request({
    url: '/site/project/list',
    method: 'post',
    data: data
  })
}
// 获取项目管理详情
export function siteProjectdetail(id) {
  return request({
    url: '/site/project/get/' + parseStrEmpty(id),
    method: 'get'
  })
}
// 新增项目管理
export function siteProjectSave(data) {
  return request({
    url: '/site/project/save',
    method: 'post',
    data: data
  })
}

// 修改项目管理
export function siteProjectUpdate(reqDTO) {
  return request({
    url: '/site/project/update',
    method: 'put',
    data: reqDTO
  })
}

// 删除项目管理
export function siteProjectRemove(id) {
  return request({
    url: '/site/project/remove/' + id,
    method: 'delete'
  })
}
