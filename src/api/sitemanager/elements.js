import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 获取站点降水量分页列表
export function sitepptnlist(data) {
  return request({
    url: '/site/pptn/list',
    method: 'post',
    data: data
  })
}
// 获取站点降水量到报率
export function sitepptnDbRate(data) {
  return request({
    url: '/site/pptn/getDbRate',
    method: 'post',
    data: data
  })
}


// 获取河道水情列表 
export function siteriverlist(data) {
  return request({
    url: '/site/river/list',
    method: 'post',
    data: data
  })
}
// 获取站点河道水情到报率
export function siteriverDbRate(data) {
  return request({
    url: '/site/river/getDbRate',
    method: 'post',
    data: data
  })
}



// 获取水库水情列表
export function sitersvrlist(data) {
  return request({
    url: '/site/rsvr/list',
    method: 'post',
    data: data
  })
}

// 获取站点图像分页列表
export function siteimglist(data) {
  return request({
    url: '/site/img/list',
    method: 'post',
    data: data
  })
}

// 获取站点图像到报率
export function siteimgDbRate(data) {
  return request({
    url: '/site/img/getDbRate',
    method: 'post',
    data: data
  })
}
