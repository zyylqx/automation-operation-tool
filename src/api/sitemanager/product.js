import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 获取产品分页列表
export function siteProductList(data) {
  return request({
    url: '/site/product/list',
    method: 'post',
    data: data
  })
}
// 获取产品详情
export function siteProductGet(id) {
  return request({
    url: '/site/product/get/' + parseStrEmpty(id),
    method: 'get'
  })
}
// 新增产品
export function siteProductSave(data) {
  return request({
    url: '/site/product/save',
    method: 'post',
    data: data
  })
}

// 修改产品
export function siteProductUpdate(reqDTO) {
  return request({
    url: '/site/product/update',
    method: 'put',
    data: reqDTO
  })
}

// 删除产品
export function siteProductRemove(id) {
  return request({
    url: '/site/product/remove/' + id,
    method: 'delete'
  })
}

// 绑定站点
export function siteProjectBindSite(data) {
  return request({
    url: '/site/project/bindSite',
    method: 'post',
    data: data
  })
}

// 取消绑定
export function siteProjectUnbindSite(data) {
  return request({
    url: '/site/project/unbindSite',
    method: 'post',
    data: data
  })
}
