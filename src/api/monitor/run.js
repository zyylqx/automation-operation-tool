import request from '@/utils/request'

// 查询站点在线状态统计
export function siteStatisticRate(data) {
  return request({
    url: '/site/statistic/rate',
    method: 'post',
    data: data
  })
}

// 查询在线率统计数据记录
export function getOnlineRateInfo(data) {
  return request({
    url: '/site/statistic/getOnlineRateInfo',
    method: 'post',
    data: data
  })
}

// 查询在保率统计数据记录
export function getInsuranceRateInfo(data) {
  return request({
    url: '/site/statistic/getInsuranceRateInfo',
    method: 'post',
    data: data
  })
}

// 查询到报率统计数据记录
export function getDbRateInfo(data) {
  return request({
    url: '/site/statistic/getDbRateInfo',
    method: 'post',
    data: data
  })
}
