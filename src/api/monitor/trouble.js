import request from '@/utils/request'

// 站点故障 分页查询
export function siteTroublePage(data) {
  return request({
    url: '/site/trouble/page',
    method: 'post',
    data: data
  })
}

// 新增站点故障
export function siteTroubleCreate(data) {
  return request({
    url: '/site/trouble/create',
    method: 'post',
    data: data
  })
}

// 更新站点故障
export function siteTroubleUpdate(data) {
  return request({
    url: '/site/trouble/update',
    method: 'post',
    data: data
  })
}

// 站点故障详情
export function siteTroubleDetail(id) {
  return request({
    url: '/site/trouble/detail/' + id,
    method: 'get'
  })
}

// 故障确认
export function siteTroubleConfirm(id) {
  return request({
    url: '/site/trouble/confirm/' + id,
    method: 'put'
  })
}
