import request from '@/utils/request'

// 登录方法
export function login(loginName, password, code, uuid) {
  
  const data = {
    loginName,
    password,
    code,
    uuid
  }
  console.log(data)
  return request({
    url: '/auth/login',
    headers: {
      isToken: false
    },
    method: 'post',
    data: data
  })
}

// 注册方法
export function register(data) {
  return request({
    url: '/register',
    headers: {
      isToken: false
    },
    method: 'post',
    data: data
  })
}

// 获取用户详细信息
export function getInfo() {
  return request({
    url: '/system/index/info',
    method: 'get'
  })
}

// 退出方法
export function logout() {
  return request({
    url: '/system/index/logout',
    method: 'post'
  })
}

// 获取验证码
export function getCodeImg() {
  return request({
    url: '/captchaImage',
    headers: {
      isToken: false
    },
    method: 'get',
    timeout: 20000
  })
}

// 文件上传管理
export const fileUpload = (data) => {
  return request({
    url: '/optmt/file/fileUpload',
    method: 'post',
    data: data
  })
}
export const optmtFileFileUploadOne = (data) => {
  return request({
    url: '/optmt/file/fileUploadOne',
    method: 'post',
    data: data
  })
}