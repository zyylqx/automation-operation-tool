import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 查询用户列表
export function listUser(data) {
  return request({
    url: '/system/user/index',
    method: 'post',
    data: data
  })
}

// 查询用户详细
export function getUser(id) {
  console.log(id)
  return request({
    url: '/system/user/get/' + parseStrEmpty(id),
    method: 'get'
  })
}

// 新增用户
export function addUser(data) {
  return request({
    url: '/system/user/save',
    method: 'post',
    data: data
  })
}

// 修改用户
export function updateUser(data) {
  return request({
    url: '/system/user/update',
    method: 'put',
    data: data
  })
}

// 删除用户
export function delUser(id) {
  return request({
    url: '/system/user/remove/' + id,
    method: 'delete'
  })
}

// 用户密码重置
export function resetUserPwd(id) {
  const data = {
    id
  }
  return request({
    url: '/system/user/resetPwd',
    method: 'put',
    data: data
  })
}

// 用户状态修改
export function changeUserStatus(id, status) {
  const data = {
    id,
    status
  }
  return request({
    url: '/system/user/changeStatus',
    method: 'put',
    data: data
  })
}

// 查询用户个人信息 
export function getUserProfile() {
  return request({
    url: '/system/user/profile',
    method: 'get'
  })
}

// 修改用户个人信息
export function updateUserProfile(data) {
  return request({
    url: '/system/user/profile',
    method: 'put',
    data: data
  })
}

// 用户密码修改
export function updateUserPwd(data) {
  return request({
    url: '/system/user/updatePwd',
    method: 'post',
    data: data
  })
}
// 用户密码修改
// export function updateUserPwd(oldPassword, password) {
//   const data = {
//     password,
//     oldPassword
//   }
//   return request({
//     url: '/system/user/updatePwd',
//     method: 'put',
//     params: data
//   })
// }

// 用户头像上传
export function uploadAvatar(data) {
  return request({
    url: '/system/user/profile/avatar',
    method: 'post',
    data: data
  })
}

// 查询授权角色
export function getAuthRole(id) {
  return request({
    url: '/system/user/authRole/' + id,
    method: 'get'
  })
}

// 保存授权角色
export function updateAuthRole(data) {
  return request({
    url: '/system/user/authRole',
    method: 'put',
    params: data
  })
}

// 查询部门下拉树结构
export function deptTreeSelect() {
  return request({
    url: '/system/user/deptTree',
    method: 'get'
  })
}
