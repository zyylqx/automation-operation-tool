import request from '@/utils/request'

// 查询日志列表
export function logPage(data) {
  return request({
    url: '/system/log/index',
    method: 'post',
    data: data
  })
}


// 查询日志详细
export function logDetail(id) {
  return request({
    url: '/system/log/get/' + id,
    method: 'get'
  })
}


// 删除日志
export function logDelete(id) {
  return request({
    url: '/system/log/remove/' + id,
    method: 'delete'
  })
}
