import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 基础信息-小流域河道关联关系 : Rv Ws Config ControllerShow/HideList OperationsExpand Operations
// post /mtool/rv/ws/config/create新增小流域河道关联关系
// delete /mtool/rv/ws/config/delete/{wscd} 删除小流域河道关联关系
// post /mtool/rv/ws/config/download下载模板
// post /mtool/rv/ws/config/export导出
// post /mtool/rv/ws/config/import导入
// post /mtool/rv/ws/config/page分页查询
// post /mtool/rv/ws/config/update更新小流域河道关联关系
// 分页查询
export function mtoolrvwsconfigPage(data) {
  return request({
    url: '/mtool/rv/ws/config/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolrvwsconfigCreate(data) {
  return request({
    url: '/mtool/rv/ws/config/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolrvwsconfigUpdate(data) {
  return request({
    url: '/mtool/rv/ws/config/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtoolrvwsconfigDelete(id) {
  return request({
    url: '/mtool/rv/ws/config/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtoolrvwsconfigDownload() {
  return request({
    url: '/mtool/rv/ws/config/download',
    method: 'get',
  })
}
// 导出
export function mtoolrvwsconfigExport(data) {
  return request({
    url: '/mtool/rv/ws/config/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolrvwsconfigImport(data) {
  return request({
    url: '/mtool/rv/ws/config/import',
    method: 'post',
    data: data
  })
}
