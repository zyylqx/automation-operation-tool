import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 基础信息-防汛责任人
// post /mtool/target/sms/create新增防汛责任人

// delete /mtool/target/sms/delete/{id}删除防汛责任人

// get /mtool/target/sms/download下载模板

// post /mtool/target/sms/export导出

// post /mtool/target/sms/import导入

// post /mtool/target/sms/page分页查询

// post /mtool/target/sms/update更新防汛责任人

// 分页查询
export function mtooltargetsmsPage(data) {
  return request({
    url: '/mtool/target/sms/page',
    method: 'post',
    data: data
  })
}
// 新增防汛责任人
export function mtooltargetsmsCreate(data) {
  return request({
    url: '/mtool/target/sms/create',
    method: 'post',
    data: data
  })
}

// 更新防汛责任人
export function mtooltargetsmsUpdate(data) {
  return request({
    url: '/mtool/target/sms/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtooltargetsmsDelete(id) {
  return request({
    url: '/mtool/target/sms/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooltargetsmsDownload() {
  return request({
    url: '/mtool/target/sms/download',
    method: 'get',
  })
}
// 导出
export function mtooltargetsmsExport(data) {
  return request({
    url: '/mtool/target/sms/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooltargetsmsImport(data) {
  return request({
    url: '/mtool/target/sms/import',
    method: 'post',
    data: data
  })
}
