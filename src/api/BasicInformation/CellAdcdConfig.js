import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 基础信息-入户调查 : Cell Adcd Config ControllerShow/HideList OperationsExpand Operations
// post /mtool/cell/adcd/config/create新增入户调查
// delete /mtool/cell/adcd/config/delete/{id}删除入户调查
// post /mtool/cell/adcd/config/download下载模板
// post /mtool/cell/adcd/config/export导出
// post /mtool/cell/adcd/config/import导入
// post /mtool/cell/adcd/config/page分页查询
// post /mtool/cell/adcd/config/update更新入户调查
// 分页查询
export function mtoolcelladcdconfigPage(data) {
  return request({
    url: '/mtool/cell/adcd/config/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolcelladcdconfigCreate(data) {
  return request({
    url: '/mtool/cell/adcd/config/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolcelladcdconfigUpdate(data) {
  return request({
    url: '/mtool/cell/adcd/config/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtoolcelladcdconfigDelete(id) {
  return request({
    url: '/mtool/cell/adcd/config/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtoolcelladcdconfigDownload() {
  return request({
    url: '/mtool/cell/adcd/config/download',
    method: 'get',
  })
}
// 导出
export function mtoolcelladcdconfigExport(data) {
  return request({
    url: '/mtool/cell/adcd/config/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolcelladcdconfigImport(data) {
  return request({
    url: '/mtool/cell/adcd/config/import',
    method: 'post',
    data: data
  })
}
