import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// post /mtool/sys/wsinfo/create新增大流域基本信息
// delete /mtool/sys/wsinfo/delete/{wswscd}删除大流域基本信息
// post /mtool/sys/wsinfo/download下载模板
// post /mtool/sys/wsinfo/export导出
// post /mtool/sys/wsinfo/import导入
// post /mtool/sys/wsinfo/page分页查询
// post /mtool/sys/wsinfo/update 更新大流域基本信息
// 分页查询
export function mtoolsyswsinfoPage(data) {
  return request({
    url: '/mtool/sys/wsinfo/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolsyswsinfoCreate(data) {
  return request({
    url: '/mtool/sys/wsinfo/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolsyswsinfoUpdate(data) {
  return request({
    url: '/mtool/sys/wsinfo/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtoolsyswsinfoDelete(id) {
  return request({
    url: '/mtool/sys/wsinfo/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtoolsyswsinfoDownload() {
  return request({
    url: '/mtool/sys/wsinfo/download',
    method: 'get',
  })
}
// 导出
export function mtoolsyswsinfoExport(data) {
  return request({
    url: '/mtool/sys/wsinfo/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolsyswsinfoImport(data) {
  return request({
    url: '/mtool/target/wsinfo/import',
    method: 'post',
    data: data
  })
}
