import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 基础信息-村庄河道关联关系 : Village River Config ControllerShow/HideList OperationsExpand Operations
// post /mtool/village/river/config/create新增村庄河道关联关系
// delete /mtool/village/river/config/delete/{adcd}删除村庄河道关联关系
// post /mtool/village/river/config/download下载模板
// post /mtool/village/river/config/export导出
// post /mtool/village/river/config/import导入
// post /mtool/village/river/config/page分页查询
// post /mtool/village/river/config/update更新村庄河道关联关系
// 分页查询
export function mtoolvillageriverconfigPage(data) {
  return request({
    url: '/mtool/village/river/config/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolvillageriverconfigCreate(data) {
  return request({
    url: '/mtool/village/river/config/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolvillageriverconfigUpdate(data) {
  return request({
    url: '/mtool/village/river/config/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtoolvillageriverconfigDelete(id) {
  return request({
    url: '/mtool/village/river/config/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtoolvillageriverconfigDownload() {
  return request({
    url: '/mtool/village/river/config/download',
    method: 'get',
  })
}
// 导出
export function mtoolvillageriverconfigExport(data) {
  return request({
    url: '/mtool/village/river/config/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolvillageriverconfigImport(data) {
  return request({
    url: '/mtool/village/river/config/import',
    method: 'post',
    data: data
  })
}
