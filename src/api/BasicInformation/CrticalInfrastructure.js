import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 基础信息-企事业单位 : Crtical Infrastructure ControllerShow/HideList OperationsExpand Operations
// post /mtool/crtical/infrastructure/create新增企事业单位
// delete /mtool/crtical/infrastructure/delete/{id}删除企事业单位
// post /mtool/crtical/infrastructure/download下载模板
// post /mtool/crtical/infrastructure/export导出
// post /mtool/crtical/infrastructure/import导入
// post /mtool/crtical/infrastructure/page分页查询
// post /mtool/crtical/infrastructure/update更新企事业单位
// 分页查询
export function mtoolcrticalinfrastructurePage(data) {
  return request({
    url: '/mtool/crtical/infrastructure/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolcrticalinfrastructureCreate(data) {
  return request({
    url: '/mtool/crtical/infrastructure/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolcrticalinfrastructureUpdate(data) {
  return request({
    url: '/mtool/crtical/infrastructure/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtoolcrticalinfrastructureDelete(id) {
  return request({
    url: '/mtool/crtical/infrastructure/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtoolcrticalinfrastructureDownload() {
  return request({
    url: '/mtool/crtical/infrastructure/download',
    method: 'get',
  })
}
// 导出
export function mtoolcrticalinfrastructureExport(data) {
  return request({
    url: '/mtool/crtical/infrastructure/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolcrticalinfrastructureImport(data) {
  return request({
    url: '/mtool/crtical/infrastructure/import',
    method: 'post',
    data: data
  })
}
