import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 获取报文查询分页列表
export function siteMessageList(data) {
  return request({
    url: '/search/msgLog/page',
    method: 'post',
    data: data
  })
}
// 获取报文
export function siteMessageget(id) {
  return request({
    url: '/site/message/get/' + parseStrEmpty(id),
    method: 'get'
  })
}

// 获取图片
export function siteMessagegetPic(id) {
  return request({
    url: '/site/message/getPic/' + parseStrEmpty(id),
    method: 'get'
  })
}

// 获取报文
export function searchMsgLogGet(id) {
  return request({
    url: '/search/msgLog/get/' + (id),
    method: 'get'
  })
}

// 获取指令报文
export function searchMsgLogGetMsgDown(data) {
  return request({
    url: '/search/msgLog/getMsgDown',
    method: 'post',
    data: data
  })
}