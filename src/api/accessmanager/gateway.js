import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 获取网关分页列表
export function gatewayPage(data) {
  return request({
    url: '/gateway/page',
    method: 'post',
    data: data
  })
}
// 获取网关详情
export function gatewayDetail(id) {
  return request({
    url: '/gateway/detail/' + parseStrEmpty(id),
    method: 'get'
  })
}
// 新增网关
export function gatewayCreate(data) {
  return request({
    url: '/gateway/create',
    method: 'post',
    data: data
  })
}

// 修改网关
export function gatewayUpdate(data) {
  return request({
    url: '/gateway/update',
    method: 'post',
    data: data
  })
}

// 删除网关
export function gatewayDelete(id) {
  return request({
    url: '/gateway/delete/' + id,
    method: 'delete'
  })
}

// 更新网关状态
export function gatewayUpdateSta(data) {
  return request({
    url: '/gateway/updateSta',
    method: 'put',
    data: data
  })
}

// 获取网关节点列表
export function gatewayNodelist(data) {
  return request({
    url: '/node/list',
    method: 'post',
    data: data
  })
}
