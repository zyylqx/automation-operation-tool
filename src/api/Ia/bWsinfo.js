import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 调查评价-小流域基本信息 : Ia B Wsinfo ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/wsinfo/create新增小流域基本信息
// delete /mtool/ia/wsinfo/delete/{rvcd}删除小流域基本信息
// post /mtool/ia/wsinfo/download下载模板
// post /mtool/ia/wsinfo/export导出
// post /mtool/ia/wsinfo/import导入
// post /mtool/ia/wsinfo/page分页查询
// post /mtool/ia/wsinfo/update更新小流域基本信息
// 分页查询
export function mtooliawsinfoPage(data) {
  return request({
    url: '/mtool/ia/wsinfo/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtooliawsinfoCreate(data) {
  return request({
    url: '/mtool/ia/wsinfo/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtooliawsinfoUpdate(data) {
  return request({
    url: '/mtool/ia/wsinfo/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtooliawsinfoDelete(id) {
  return request({
    url: '/mtool/ia/wsinfo/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooliawsinfoDownload() {
  return request({
    url: '/mtool/ia/wsinfo/download',
    method: 'get',
  })
}
// 导出
export function mtooliawsinfoExport(data) {
  return request({
    url: '/mtool/ia/wsinfo/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooliawsinfoImport(data) {
  return request({
    url: '/mtool/ia/wsinfo/import',
    method: 'post',
    data: data
  })
}
