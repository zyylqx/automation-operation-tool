import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 调查评价-沟道横断面成果 : Ia M Hsurface ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/hsurface/create新增沟道横断面成果
// delete /mtool/ia/hsurface/delete/{guid}删除沟道横断面成果
// post /mtool/ia/hsurface/download下载模板
// post /mtool/ia/hsurface/export导出
// post /mtool/ia/hsurface/import导入
// post /mtool/ia/hsurface/page分页查询
// post /mtool/ia/hsurface/update更新沟道横断面成果
// 分页查询
export function mtooliahsurfacePage(data) {
  return request({
    url: '/mtool/ia/hsurface/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtooliahsurfaceCreate(data) {
  return request({
    url: '/mtool/ia/hsurface/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtooliahsurfaceUpdate(data) {
  return request({
    url: '/mtool/ia/hsurface/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtooliahsurfaceDelete(id) {
  return request({
    url: '/mtool/ia/hsurface/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooliahsurfaceDownload() {
  return request({
    url: '/mtool/ia/hsurface/download',
    method: 'get',
  })
}
// 导出
export function mtooliahsurfaceExport(data) {
  return request({
    url: '/mtool/ia/hsurface/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooliahsurfaceImport(data) {
  return request({
    url: '/mtool/ia/hsurface/import',
    method: 'post',
    data: data
  })
}
