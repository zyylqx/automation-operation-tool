import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 调查评价-沟道纵断面成果 : Ia M Vsurface ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/vsurface/create新增沟道纵断面成果
// delete /mtool/ia/vsurface/delete/{guid}删除沟道纵断面成果
// post /mtool/ia/vsurface/download下载模板
// post /mtool/ia/vsurface/export导出
// post /mtool/ia/vsurface/import导入
// post /mtool/ia/vsurface/page分页查询
// post /mtool/ia/vsurface/update更新沟道纵断面成果
// 分页查询
export function mtooliavsurfacePage(data) {
  return request({
    url: '/mtool/ia/vsurface/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtooliavsurfaceCreate(data) {
  return request({
    url: '/mtool/ia/vsurface/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtooliavsurfaceUpdate(data) {
  return request({
    url: '/mtool/ia/vsurface/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtooliavsurfaceDelete(id) {
  return request({
    url: '/mtool/ia/vsurface/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooliavsurfaceDownload() {
  return request({
    url: '/mtool/ia/vsurface/download',
    method: 'get',
  })
}
// 导出
export function mtooliavsurfaceExport(data) {
  return request({
    url: '/mtool/ia/vsurface/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooliavsurfaceImport(data) {
  return request({
    url: '/mtool/ia/vsurface/import',
    method: 'post',
    data: data
  })
}
