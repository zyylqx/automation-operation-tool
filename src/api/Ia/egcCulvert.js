import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 调查评价-路涵工程 : Ia Egc Culvert ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/culvert/create新增调查评价路涵工程
// delete /mtool/ia/culvert/delete/{guid}删除调查评价路涵工程
// post /mtool/ia/culvert/download下载模板
// post /mtool/ia/culvert/export导出
// post /mtool/ia/culvert/import导入
// post /mtool/ia/culvert/page分页查询
// post /mtool/ia/culvert/update新调查评价路涵工程
// 分页查询
export function mtooliaculvertPage(data) {
  return request({
    url: '/mtool/ia/culvert/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtooliaculvertCreate(data) {
  return request({
    url: '/mtool/ia/culvert/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtooliaculvertUpdate(data) {
  return request({
    url: '/mtool/ia/culvert/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtooliaculvertDelete(id) {
  return request({
    url: '/mtool/ia/culvert/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooliaculvertDownload() {
  return request({
    url: '/mtool/ia/culvert/download',
    method: 'get',
  })
}
// 导出
export function mtooliaculvertExport(data) {
  return request({
    url: '/mtool/ia/culvert/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooliaculvertImport(data) {
  return request({
    url: '/mtool/ia/culvert/import',
    method: 'post',
    data: data
  })
}
