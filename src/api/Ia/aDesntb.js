import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 调查评价-小流域设计暴雨成果 : Ia A Desntb ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/desntb/create新增小流域设计暴雨成果
// delete /mtool/ia/desntb/delete/{guid}删除小流域设计暴雨成果
// post /mtool/ia/desntb/download下载模板
// post /mtool/ia/desntb/export导出
// post /mtool/ia/desntb/import导入
// post /mtool/ia/desntb/page分页查询
// post /mtool/ia/desntb/update更新小流域设计暴雨成果
// 分页查询
export function mtooliadesntbPage(data) {
  return request({
    url: '/mtool/ia/desntb/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtooliadesntbCreate(data) {
  return request({
    url: '/mtool/ia/desntb/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtooliadesntbUpdate(data) {
  return request({
    url: '/mtool/ia/desntb/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtooliadesntbDelete(id) {
  return request({
    url: '/mtool/ia/desntb/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooliadesntbDownload() {
  return request({
    url: '/mtool/ia/desntb/download',
    method: 'get',
  })
}
// 导出
export function mtooliadesntbExport(data) {
  return request({
    url: '/mtool/ia/desntb/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooliadesntbImport(data) {
  return request({
    url: '/mtool/ia/desntb/import',
    method: 'post',
    data: data
  })
}
