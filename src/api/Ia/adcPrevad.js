import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 调查评价-防治区基本情况调查成果 : Ia Adc Prevad ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/prevad/create新增防治区基本情况调查成果
// delete /mtool/ia/prevad/delete/{guid}删除防治区基本情况调查成果
// post /mtool/ia/prevad/download下载模板
// post /mtool/ia/prevad/export导出
// post /mtool/ia/prevad/import导入
// post /mtool/ia/prevad/page分页查询
// post /mtool/ia/prevad/update更新防治区基本情况调查成果
// 分页查询
export function mtooliaprevadPage(data) {
  return request({
    url: '/mtool/ia/prevad/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtooliaprevadCreate(data) {
  return request({
    url: '/mtool/ia/prevad/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtooliaprevadUpdate(data) {
  return request({
    url: '/mtool/ia/prevad/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtooliaprevadDelete(id) {
  return request({
    url: '/mtool/ia/prevad/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooliaprevadDownload() {
  return request({
    url: '/mtool/ia/prevad/download',
    method: 'get',
  })
}
// 导出
export function mtooliaprevadExport(data) {
  return request({
    url: '/mtool/ia/prevad/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooliaprevadImport(data) {
  return request({
    url: '/mtool/ia/prevad/import',
    method: 'post',
    data: data
  })
}
