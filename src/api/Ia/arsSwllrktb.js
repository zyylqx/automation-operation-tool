import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 调查评价-控制断面水位-流量-人口关系 : Ia Ars Swllrktb ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/swllrktb/create新增控制断面水位-流量-人口关系
// delete /mtool/ia/swllrktb/delete/{guid}删除控制断面水位-流量-人口关系
// post /mtool/ia/swllrktb/download下载模板
// post /mtool/ia/swllrktb/export导出
// post /mtool/ia/swllrktb/import导入
// post /mtool/ia/swllrktb/page分页查询
// post /mtool/ia/swllrktb/update更新控制断面水位-流量-人口关系


// 分页查询
export function mtooliaswllrktbPage(data) {
  return request({
    url: '/mtool/ia/swllrktb/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtooliaswllrktbCreate(data) {
  return request({
    url: '/mtool/ia/swllrktb/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtooliaswllrktbUpdate(data) {
  return request({
    url: '/mtool/ia/swllrktb/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtooliaswllrktbDelete(id) {
  return request({
    url: '/mtool/ia/swllrktb/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooliaswllrktbDownload() {
  return request({
    url: '/mtool/ia/swllrktb/download',
    method: 'get',
  })
}
// 导出
export function mtooliaswllrktbExport(data) {
  return request({
    url: '/mtool/ia/swllrktb/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooliaswllrktbImport(data) {
  return request({
    url: '/mtool/ia/swllrktb/import',
    method: 'post',
    data: data
  })
}
