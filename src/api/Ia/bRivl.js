import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 调查评价-小流域河段基本信息 : Ia B Rivl ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/rivl/create新增小流域河段基本信息
// delete /mtool/ia/rivl/delete/{rvcd}删除小流域河段基本信息
// post /mtool/ia/rivl/download下载模板
// post /mtool/ia/rivl/export导出
// post /mtool/ia/rivl/import导入
// post /mtool/ia/rivl/page分页查询
// post /mtool/ia/rivl/update更新小流域河段基本信息
// 分页查询
export function mtooliarivlPage(data) {
  return request({
    url: '/mtool/ia/rivl/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtooliarivlCreate(data) {
  return request({
    url: '/mtool/ia/rivl/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtooliarivlUpdate(data) {
  return request({
    url: '/mtool/ia/rivl/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtooliarivlDelete(id) {
  return request({
    url: '/mtool/ia/rivl/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooliarivlDownload() {
  return request({
    url: '/mtool/ia/rivl/download',
    method: 'get',
  })
}
// 导出
export function mtooliarivlExport(data) {
  return request({
    url: '/mtool/ia/rivl/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooliarivlImport(data) {
  return request({
    url: '/mtool/ia/rivl/import',
    method: 'post',
    data: data
  })
}
