import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";
// 调查评价-桥梁工程 : Ia Egc Bridge ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/bridge/create新增调查评价桥梁工程
// delete /mtool/ia/bridge/delete/{guid}删除调查评价桥梁工程
// post /mtool/ia/bridge/download下载模板
// post /mtool/ia/bridge/export导出
// post /mtool/ia/bridge/import导入
// post /mtool/ia/bridge/page分页查询
// post /mtool/ia/bridge/update更新调查评价桥梁工程
// 分页查询
export function mtooliabridgePage(data) {
  return request({
    url: '/mtool/ia/bridge/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtooliabridgeCreate(data) {
  return request({
    url: '/mtool/ia/bridge/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtooliabridgeUpdate(data) {
  return request({
    url: '/mtool/ia/bridge/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtooliabridgeDelete(id) {
  return request({
    url: '/mtool/ia/bridge/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooliabridgeDownload() {
  return request({
    url: '/mtool/ia/bridge/download',
    method: 'get',
  })
}
// 导出
export function mtooliabridgeExport(data) {
  return request({
    url: '/mtool/ia/bridge/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooliabridgeImport(data) {
  return request({
    url: '/mtool/ia/bridge/import',
    method: 'post',
    data: data
  })
}
