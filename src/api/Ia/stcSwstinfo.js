import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 调查评价-简易水位站 : Ia Stc Swstinfo ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/swstinfo/create新增简易水位站
// delete /mtool/ia/swstinfo/delete/{guid}删除简易水位站
// post /mtool/ia/swstinfo/download下载模板
// post /mtool/ia/swstinfo/export导出
// post /mtool/ia/swstinfo/import导入
// post /mtool/ia/swstinfo/page分页查询
// post /mtool/ia/swstinfo/update更新简易水位站
// 分页查询
export function mtooliaswstinfoPage(data) {
  return request({
    url: '/mtool/ia/swstinfo/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtooliaswstinfoCreate(data) {
  return request({
    url: '/mtool/ia/swstinfo/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtooliaswstinfoUpdate(data) {
  return request({
    url: '/mtool/ia/swstinfo/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtooliaswstinfoDelete(id) {
  return request({
    url: '/mtool/ia/swstinfo/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooliaswstinfoDownload() {
  return request({
    url: '/mtool/ia/swstinfo/download',
    method: 'get',
  })
}
// 导出
export function mtooliaswstinfoExport(data) {
  return request({
    url: '/mtool/ia/swstinfo/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooliaswstinfoImport(data) {
  return request({
    url: '/mtool/ia/swstinfo/import',
    method: 'post',
    data: data
  })
}
