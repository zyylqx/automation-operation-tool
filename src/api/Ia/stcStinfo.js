import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 调查评价-自动监测站点 : Ia Stc Stinfo ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/stinfo/create新增自动监测站点
// delete /mtool/ia/stinfo/delete/{guid}删除自动监测站点
// post /mtool/ia/stinfo/download下载模板
// post /mtool/ia/stinfo/export导出
// post /mtool/ia/stinfo/import导入
// post /mtool/ia/stinfo/page分页查询
// post /mtool/ia/stinfo/update更新自动监测站点
// 分页查询
export function mtooliastinfoPage(data) {
  return request({
    url: '/mtool/ia/stinfo/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtooliastinfoCreate(data) {
  return request({
    url: '/mtool/ia/stinfo/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtooliastinfoUpdate(data) {
  return request({
    url: '/mtool/ia/stinfo/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtooliastinfoDelete(id) {
  return request({
    url: '/mtool/ia/stinfo/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooliastinfoDownload() {
  return request({
    url: '/mtool/ia/stinfo/download',
    method: 'get',
  })
}
// 导出
export function mtooliastinfoExport(data) {
  return request({
    url: '/mtool/ia/stinfo/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooliastinfoImport(data) {
  return request({
    url: '/mtool/ia/stinfo/import',
    method: 'post',
    data: data
  })
}
