import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";
// 调查评价-预警指标时段雨量成果 : Ia A Dfwrule ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/dfwrule/create新增预警指标时段雨量成果
// delete /mtool/ia/dfwrule/delete/{guid}删除预警指标时段雨量成果
// post /mtool/ia/dfwrule/download下载模板
// post /mtool/ia/dfwrule/export导出
// post /mtool/ia/dfwrule/import导入
// post /mtool/ia/dfwrule/page分页查询
// post /mtool/ia/dfwrule/update更新预警指标时段雨量成果
// 分页查询
export function mtooliadfwrulePage(data) {
  return request({
    url: '/mtool/ia/dfwrule/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtooliadfwruleCreate(data) {
  return request({
    url: '/mtool/ia/dfwrule/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtooliadfwruleUpdate(data) {
  return request({
    url: '/mtool/ia/dfwrule/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtooliadfwruleDelete(id) {
  return request({
    url: '/mtool/ia/dfwrule/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooliadfwruleDownload() {
  return request({
    url: '/mtool/ia/dfwrule/download',
    method: 'get',
  })
}
// 导出
export function mtooliadfwruleExport(data) {
  return request({
    url: '/mtool/ia/dfwrule/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooliadfwruleImport(data) {
  return request({
    url: '/mtool/ia/dfwrule/import',
    method: 'post',
    data: data
  })
}
