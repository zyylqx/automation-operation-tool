import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 调查评价-无线预警广播站 : Ia Stc Wbrinfo ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/wbrinfo/create新增无线预警广播站
// delete /mtool/ia/wbrinfo/delete/{guid}删除无线预警广播站
// post /mtool/ia/wbrinfo/download下载模板
// post /mtool/ia/wbrinfo/export导出
// post /mtool/ia/wbrinfo/import导入
// post /mtool/ia/wbrinfo/page分页查询
// post /mtool/ia/wbrinfo/update更新无线预警广播站
// 分页查询
export function mtooliawbrinfoPage(data) {
  return request({
    url: '/mtool/ia/wbrinfo/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtooliawbrinfoCreate(data) {
  return request({
    url: '/mtool/ia/wbrinfo/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtooliawbrinfoUpdate(data) {
  return request({
    url: '/mtool/ia/wbrinfo/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtooliawbrinfoDelete(id) {
  return request({
    url: '/mtool/ia/wbrinfo/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooliawbrinfoDownload() {
  return request({
    url: '/mtool/ia/wbrinfo/download',
    method: 'get',
  })
}
// 导出
export function mtooliawbrinfoExport(data) {
  return request({
    url: '/mtool/ia/wbrinfo/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooliawbrinfoImport(data) {
  return request({
    url: '/mtool/ia/wbrinfo/import',
    method: 'post',
    data: data
  })
}
