import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 调查评价-安置点 : Ia A Placement ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/placement/create新增安置点
// delete /mtool/ia/placement/delete/{tscd}删除安置点
// post /mtool/ia/placement/download下载模板
// post /mtool/ia/placement/export导出
// post /mtool/ia/placement/import导入
// post /mtool/ia/placement/page分页查询
// post /mtool/ia/placement/update更新安置点
// 分页查询
export function mtooliaplacementPage(data) {
  return request({
    url: '/mtool/ia/placement/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtooliaplacementCreate(data) {
  return request({
    url: '/mtool/ia/placement/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtooliaplacementUpdate(data) {
  return request({
    url: '/mtool/ia/placement/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtooliaplacementDelete(id) {
  return request({
    url: '/mtool/ia/placement/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooliaplacementDownload() {
  return request({
    url: '/mtool/ia/placement/download',
    method: 'get',
  })
}
// 导出
export function mtooliaplacementExport(data) {
  return request({
    url: '/mtool/ia/placement/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooliaplacementImport(data) {
  return request({
    url: '/mtool/ia/placement/import',
    method: 'post',
    data: data
  })
}
