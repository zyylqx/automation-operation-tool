import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 调查评价-控制断面设计洪水成果 : Ia A Sdtdtb ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/sdtdtb/create新增控制断面设计洪水成果
// delete /mtool/ia/sdtdtb/delete/{guid}删除控制断面设计洪水成果
// post /mtool/ia/sdtdtb/download下载模板
// post /mtool/ia/sdtdtb/export导出
// post /mtool/ia/sdtdtb/import导入
// post /mtool/ia/sdtdtb/page分页查询
// post /mtool/ia/sdtdtb/update更新控制断面设计洪水成果
// 分页查询
export function mtooliasdtdtbPage(data) {
  return request({
    url: '/mtool/ia/sdtdtb/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtooliasdtdtbCreate(data) {
  return request({
    url: '/mtool/ia/sdtdtb/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtooliasdtdtbUpdate(data) {
  return request({
    url: '/mtool/ia/sdtdtb/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtooliasdtdtbDelete(id) {
  return request({
    url: '/mtool/ia/sdtdtb/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooliasdtdtbDownload() {
  return request({
    url: '/mtool/ia/sdtdtb/download',
    method: 'get',
  })
}
// 导出
export function mtooliasdtdtbExport(data) {
  return request({
    url: '/mtool/ia/sdtdtb/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooliasdtdtbImport(data) {
  return request({
    url: '/mtool/ia/sdtdtb/import',
    method: 'post',
    data: data
  })
}
