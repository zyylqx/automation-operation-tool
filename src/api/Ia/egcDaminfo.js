import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 调查评价-塘（堰）坝工程 : Ia Egc Daminfo ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/daminfo/create新增调查评价塘（堰）坝工程
// delete /mtool/ia/daminfo/delete/{guid}删除调查评价塘（堰）坝工程
// post /mtool/ia/daminfo/download下载模板
// post /mtool/ia/daminfo/export导出
// post /mtool/ia/daminfo/import导入
// post /mtool/ia/daminfo/page分页查询
// post /mtool/ia/daminfo/update更新调查评价塘（堰）坝工程
// 分页查询
export function mtooliadaminfoPage(data) {
  return request({
    url: '/mtool/ia/daminfo/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtooliadaminfoCreate(data) {
  return request({
    url: '/mtool/ia/daminfo/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtooliadaminfoUpdate(data) {
  return request({
    url: '/mtool/ia/daminfo/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtooliadaminfoDelete(id) {
  return request({
    url: '/mtool/ia/daminfo/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooliadaminfoDownload() {
  return request({
    url: '/mtool/ia/daminfo/download',
    method: 'get',
  })
}
// 导出
export function mtooliadaminfoExport(data) {
  return request({
    url: '/mtool/ia/daminfo/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooliadaminfoImport(data) {
  return request({
    url: '/mtool/ia/daminfo/import',
    method: 'post',
    data: data
  })
}
