import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 调查评价-沟道纵断面测量点 : Ia M Vspoint ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/vspoint/create新增沟道纵断面测量点
// delete /mtool/ia/vspoint/delete/{guid}删除沟道纵断面测量点
// post /mtool/ia/vspoint/download下载模板
// post /mtool/ia/vspoint/export导出
// post /mtool/ia/vspoint/import导入
// post /mtool/ia/vspoint/page分页查询
// post /mtool/ia/vspoint/update更新沟道纵断面测量点
// 分页查询
export function mtooliavspointPage(data) {
  return request({
    url: '/mtool/ia/vspoint/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtooliavspointCreate(data) {
  return request({
    url: '/mtool/ia/vspoint/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtooliavspointUpdate(data) {
  return request({
    url: '/mtool/ia/vspoint/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtooliavspointDelete(id) {
  return request({
    url: '/mtool/ia/vspoint/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooliavspointDownload() {
  return request({
    url: '/mtool/ia/vspoint/download',
    method: 'get',
  })
}
// 导出
export function mtooliavspointExport(data) {
  return request({
    url: '/mtool/ia/vspoint/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooliavspointImport(data) {
  return request({
    url: '/mtool/ia/vspoint/import',
    method: 'post',
    data: data
  })
}
