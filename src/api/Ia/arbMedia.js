import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 调查评价-多媒体资料信息 : Ia Arb Media ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/media/create新增多媒体资料信息
// delete /mtool/ia/media/delete/{guid}删除多媒体资料信息
// post /mtool/ia/media/download下载模板
// post /mtool/ia/media/export导出
// post /mtool/ia/media/import导入
// post /mtool/ia/media/page分页查询
// post /mtool/ia/media/update更新多媒体资料信息
// 分页查询
export function mtooliamediaPage(data) {
  return request({
    url: '/mtool/ia/media/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtooliamediaCreate(data) {
  return request({
    url: '/mtool/ia/media/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtooliamediaUpdate(data) {
  return request({
    url: '/mtool/ia/media/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtooliamediaDelete(id) {
  return request({
    url: '/mtool/ia/media/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooliamediaDownload() {
  return request({
    url: '/mtool/ia/media/download',
    method: 'get',
  })
}
// 导出
export function mtooliamediaExport(data) {
  return request({
    url: '/mtool/ia/media/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooliamediaImport(data) {
  return request({
    url: '/mtool/ia/media/import',
    method: 'post',
    data: data
  })
}
