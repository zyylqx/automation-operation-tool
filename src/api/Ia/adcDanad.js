import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 调查评价-调查评价危险区 : Ia Adc Danad ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/danad/create新增调查评价危险区
// delete /mtool/ia/danad/delete/{guid}删除调查评价危险区
// post /mtool/ia/danad/download下载模板
// post /mtool/ia/danad/export导出
// post /mtool/ia/danad/import导入
// post /mtool/ia/danad/page分页查询
// post /mtool/ia/danad/update更新调查评价危险区
// 分页查询
export function mtooliadanadPage(data) {
  return request({
    url: '/mtool/ia/danad/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtooliadanadCreate(data) {
  return request({
    url: '/mtool/ia/danad/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtooliadanadUpdate(data) {
  return request({
    url: '/mtool/ia/danad/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtooliadanadDelete(id) {
  return request({
    url: '/mtool/ia/danad/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooliadanadDownload() {
  return request({
    url: '/mtool/ia/danad/download',
    method: 'get',
  })
}
// 导出
export function mtooliadanadExport(data) {
  return request({
    url: '/mtool/ia/danad/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooliadanadImport(data) {
  return request({
    url: '/mtool/ia/danad/import',
    method: 'post',
    data: data
  })
}
