import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 调查评价-简易雨量站 : Ia Stc Srstinfo ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/srstinfo/create新增简易雨量站
// delete /mtool/ia/srstinfo/delete/{guid}删除简易雨量站
// post /mtool/ia/srstinfo/download下载模板
// post /mtool/ia/srstinfo/export导出
// post /mtool/ia/srstinfo/import导入
// post /mtool/ia/srstinfo/page分页查询
// post /mtool/ia/srstinfo/update更新简易雨量站
// 分页查询
export function mtooliasrstinfoPage(data) {
  return request({
    url: '/mtool/ia/srstinfo/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtooliasrstinfoCreate(data) {
  return request({
    url: '/mtool/ia/srstinfo/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtooliasrstinfoUpdate(data) {
  return request({
    url: '/mtool/ia/srstinfo/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtooliasrstinfoDelete(id) {
  return request({
    url: '/mtool/ia/srstinfo/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooliasrstinfoDownload() {
  return request({
    url: '/mtool/ia/srstinfo/download',
    method: 'get',
  })
}
// 导出
export function mtooliasrstinfoExport(data) {
  return request({
    url: '/mtool/ia/srstinfo/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooliasrstinfoImport(data) {
  return request({
    url: '/mtool/ia/srstinfo/import',
    method: 'post',
    data: data
  })
}
