import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 调查评价-调查评价村庄 : Ia Adc Adinfo ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/adinfo/create新增调查评价村庄
// delete /mtool/ia/adinfo/delete/{guid}删除调查评价村庄
// post /mtool/ia/adinfo/download下载模板
// post /mtool/ia/adinfo/export导出
// post /mtool/ia/adinfo/import导入
// post /mtool/ia/adinfo/page分页查询
// post /mtool/ia/adinfo/update更新调查评价村庄
// 分页查询
export function mtooliaadinfoPage(data) {
  return request({
    url: '/mtool/ia/adinfo/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtooliaadinfoCreate(data) {
  return request({
    url: '/mtool/ia/adinfo/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtooliaadinfoUpdate(data) {
  return request({
    url: '/mtool/ia/adinfo/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtooliaadinfoDelete(id) {
  return request({
    url: '/mtool/ia/adinfo/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooliaadinfoDownload() {
  return request({
    url: '/mtool/ia/adinfo/download',
    method: 'get',
  })
}
// 导出
export function mtooliaadinfoExport(data) {
  return request({
    url: '/mtool/ia/adinfo/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooliaadinfoImport(data) {
  return request({
    url: '/mtool/ia/adinfo/import',
    method: 'post',
    data: data
  })
}
