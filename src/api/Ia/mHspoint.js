import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 调查评价-沟道横断面测量点 : Ia M Hspoint ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/hspoint/create 新增沟道横断面测量点
// delete /mtool/ia/hspoint/delete/{guid} 删除沟道横断面测量点
// post /mtool/ia/hspoint/download 下载模板
// post /mtool/ia/hspoint/export 导出
// post /mtool/ia/hspoint/import 导入
// post /mtool/ia/hspoint/page 分页查询
// post /mtool/ia/hspoint/update 更新沟道横断面测量点
// 分页查询
export function mtooliahspointPage(data) {
  return request({
    url: '/mtool/ia/hspoint/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtooliahspointCreate(data) {
  return request({
    url: '/mtool/ia/hspoint/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtooliahspointUpdate(data) {
  return request({
    url: '/mtool/ia/hspoint/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtooliahspointDelete(id) {
  return request({
    url: '/mtool/ia/hspoint/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtooliahspointDownload() {
  return request({
    url: '/mtool/ia/hspoint/download',
    method: 'get',
  })
}
// 导出
export function mtooliahspointExport(data) {
  return request({
    url: '/mtool/ia/hspoint/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtooliahspointImport(data) {
  return request({
    url: '/mtool/ia/hspoint/import',
    method: 'post',
    data: data
  })
}
