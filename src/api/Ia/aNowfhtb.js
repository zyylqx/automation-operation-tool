import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 调查评价-防洪现状评价成果 : Ia A Nowfhtb ControllerShow/HideList OperationsExpand Operations
// post /mtool/ia/nowfhtb/create新增防洪现状评价成果
// delete /mtool/ia/nowfhtb/delete/{guid}删除防洪现状评价成果
// post /mtool/ia/nowfhtb/download下载模板
// post /mtool/ia/nowfhtb/export导出
// post /mtool/ia/nowfhtb/import导入
// post /mtool/ia/nowfhtb/page分页查询
// post /mtool/ia/nowfhtb/update更新防洪现状评价成果
// 分页查询
export function mtoolianowfhtbPage(data) {
  return request({
    url: '/mtool/ia/nowfhtb/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolianowfhtbCreate(data) {
  return request({
    url: '/mtool/ia/nowfhtb/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolianowfhtbUpdate(data) {
  return request({
    url: '/mtool/ia/nowfhtb/update',
    method: 'post',
    data: data
  })
}

// 删除防汛责任人
export function mtoolianowfhtbDelete(id) {
  return request({
    url: '/mtool/ia/nowfhtb/delete/' + id,
    method: 'delete'
  })
}

// 下载模板
export function mtoolianowfhtbDownload() {
  return request({
    url: '/mtool/ia/nowfhtb/download',
    method: 'get',
  })
}
// 导出
export function mtoolianowfhtbExport(data) {
  return request({
    url: '/mtool/ia/nowfhtb/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolianowfhtbImport(data) {
  return request({
    url: '/mtool/ia/nowfhtb/import',
    method: 'post',
    data: data
  })
}
