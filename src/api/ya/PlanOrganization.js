import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 预案-预案组织机构表 : Plan Organization ControllerShow/HideList OperationsExpand Operations
// post /mtool/plan/organization/create
// 新增预案组织机构
// delete /mtool/plan/organization/delete/{id}
// 删除预案组织机构
// post /mtool/plan/organization/download
// 下载模板
// post /mtool/plan/organization/export
// 导出
// post /mtool/plan/organization/import
// 导入
// post /mtool/plan/organization/page
// 分页查询
// post /mtool/plan/organization/update
// 更新预案组织机构

// 分页查询
export function mtoolplanorganizationPage(data) {
  return request({
    url: '/mtool/plan/organization/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolplanorganizationCreate(data) {
  return request({
    url: '/mtool/plan/organization/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolplanorganizationUpdate(data) {
  return request({
    url: '/mtool/plan/organization/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolplanorganizationDelete(data) {
  return request({
    url: '/mtool/plan/organization/delete/'+data.id,
    method: 'delete',

  })
}

// 下载模板
export function mtoolplanorganizationDownload() {
  return request({
    url: '/mtool/plan/organization/download',
    method: 'get',
  })
}
// 导出
export function mtoolplanorganizationExport(data) {
  return request({
    url: '/mtool/plan/organization/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolplanorganizationImport(data) {
  return request({
    url: '/mtool/plan/organization/import',
    method: 'post',
    data: data
  })
}
