import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 预案-结构化预案表 : Plan Basic ControllerShow/HideList OperationsExpand Operations
// post /mtool/plan/basic/create
// 新增结构化预案

// delete /mtool/plan/basic/delete/{id}
// 删除结构化预案

// post /mtool/plan/basic/download
// 下载模板

// post /mtool/plan/basic/export
// 导出

// post /mtool/plan/basic/import
// 导入

// post /mtool/plan/basic/page
// 分页查询

// post /mtool/plan/basic/update
// 更新结构化预案

// 分页查询
export function mtoolplanbasicPage(data) {
  return request({
    url: '/mtool/plan/basic/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolplanbasicCreate(data) {
  return request({
    url: '/mtool/plan/basic/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolplanbasicUpdate(data) {
  return request({
    url: '/mtool/plan/basic/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolplanbasicDelete(data) {
  return request({
    url: '/mtool/plan/basic/delete/'+data.id,
    method: 'delete',

  })
}

// 下载模板
export function mtoolplanbasicDownload() {
  return request({
    url: '/mtool/plan/basic/download',
    method: 'get',
  })
}
// 导出
export function mtoolplanbasicExport(data) {
  return request({
    url: '/mtool/plan/basic/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolplanbasicImport(data) {
  return request({
    url: '/mtool/plan/basic/import',
    method: 'post',
    data: data
  })
}
