import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 预案-预案组织机构人员表 : Plan Organization User ControllerShow/HideList OperationsExpand Operations
// post /mtool/plan/organization/user/create新增预案组织机构人员
// delete /mtool/plan/organization/user/delete/{id}删除预案组织机构人员
// post /mtool/plan/organization/user/download下载模板
// post /mtool/plan/organization/user/export导出
// post /mtool/plan/organization/user/import导入
// post /mtool/plan/organization/user/page分页查询
// post /mtool/plan/organization/user/update更新预案组织机构人员

// 分页查询
export function mtoolplanorganizationuserPage(data) {
  return request({
    url: '/mtool/plan/organization/user/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolplanorganizationuserCreate(data) {
  return request({
    url: '/mtool/plan/organization/user/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolplanorganizationuserUpdate(data) {
  return request({
    url: '/mtool/plan/organization/user/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolplanorganizationuserDelete(data) {
  return request({
    url: '/mtool/plan/organization/user/delete/'+data.id,
    method: 'delete'
  })
}

// 下载模板
export function mtoolplanorganizationuserDownload() {
  return request({
    url: '/mtool/plan/organization/user/download',
    method: 'get',
  })
}
// 导出
export function mtoolplanorganizationuserExport(data) {
  return request({
    url: '/mtool/plan/organization/user/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolplanorganizationuserImport(data) {
  return request({
    url: '/mtool/plan/organization/user/import',
    method: 'post',
    data: data
  })
}
