import request from '@/utils/request'
import {
  parseStrEmpty
} from "@/utils/ruoyi";

// 预案-防汛预案表 : Ew Addvcd Floodplan ControllerShow/HideList OperationsExpand Operations
// post /mtool/ew/addvcd/floodplan/create
// 新增防汛预案

// delete /mtool/ew/addvcd/floodplan/delete/{id}
// 删除防汛预案

// post /mtool/ew/addvcd/floodplan/download
// 下载模板

// post /mtool/ew/addvcd/floodplan/export
// 导出

// post /mtool/ew/addvcd/floodplan/import
// 导入

// post /mtool/ew/addvcd/floodplan/page
// 分页查询

// post /mtool/ew/addvcd/floodplan/update
// 更新防汛预案

// 分页查询
export function mtoolewaddvcdfloodplanPage(data) {
  return request({
    url: '/mtool/ew/addvcd/floodplan/page',
    method: 'post',
    data: data
  })
}
// 新增
export function mtoolewaddvcdfloodplanCreate(data) {
  return request({
    url: '/mtool/ew/addvcd/floodplan/create',
    method: 'post',
    data: data
  })
}

// 更新
export function mtoolewaddvcdfloodplanUpdate(data) {
  return request({
    url: '/mtool/ew/addvcd/floodplan/update',
    method: 'post',
    data: data
  })
}

// 删除
export function mtoolewaddvcdfloodplanDelete(data) {
  return request({
    url: '/mtool/ew/addvcd/floodplan/delete/'+data.id,
    method: 'delete',
  })
}

// 下载模板
export function mtoolewaddvcdfloodplanDownload() {
  return request({
    url: '/mtool/ew/addvcd/floodplan/download',
    method: 'get',
  })
}
// 导出
export function mtoolewaddvcdfloodplanExport(data) {
  return request({
    url: '/mtool/ew/addvcd/floodplan/export',
    method: 'put',
    data: data
  })
}

// 导入
export function mtoolewaddvcdfloodplanImport(data) {
  return request({
    url: '/mtool/ew/addvcd/floodplan/import',
    method: 'post',
    data: data
  })
}
