import Cookies from 'js-cookie'

const port = window.location.port || '80'
const TokenKey = 'Admin-Token' + port

console.log(TokenKey)

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}
