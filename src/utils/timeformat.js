
export function formatTimeAll(time) {
    const datetime= new Date(time);
    const year = datetime.getFullYear();
    const month = String(datetime.getMonth() + 1).padStart(2,'0');
    const day = String(datetime.getDate()).padStart(2, '0');
    const hours = String(datetime.getHours()).padStart(2, '0');
    const minutes = String(datetime.getMinutes()).padStart(2, '0');
    return `${year}-${month}-${day} ${hours}:${minutes}`;
}

export function formatTimeday(time) {
    if(time !== null || time !=undefined){
        const datetime= new Date(time);
        const year = datetime.getFullYear();
        const month = String(datetime.getMonth() + 1).padStart(2,'0');
        const day = String(datetime.getDate()).padStart(2, '0');
        return `${year}-${month}-${day}`;
    }else{
        return null
    }
    
}
export function formatTimeMouth(time) {
    if(time !== null || time !=undefined){
        const datetime= new Date(time);
        const month = String(datetime.getMonth() + 1).padStart(2,'0');
        const day = String(datetime.getDate()).padStart(2, '0');
        return `${month}-${day}`;
    }else{
        return null
    }
}

export function formatTimeMinutes(time) {
    const datetime= new Date(time);
    const hours = String(datetime.getHours()).padStart(2, '0');
    const minutes = String(datetime.getMinutes()).padStart(2, '0');
    return `${hours}:${minutes}`;
}

export function formatTimedayPlan(time) {
    if(time !== null || time !=undefined){
        const datetime= new Date(time);
        const year = datetime.getFullYear();
        const month = String(datetime.getMonth() + 1).padStart(2,'0');
        const day = String(datetime.getDate()).padStart(2, '0');
        return `${year}${month}${day}`;
    }else{
        return null
    }
}