import {
  login,
  logout,
  getInfo
} from '@/api/login'
import {
  getToken,
  setToken,
  removeToken
} from '@/utils/auth'

const status = {
  state: {
    // 建议处理方式
    disposalList:[
      {
        label: "检修",
        value: "0",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "更换",
        value: "1",
        raw: {
          listClass: "success",
        },
      },
      {
        label: "其他",
        value: "2",
        raw: {
          listClass: "primary",
        },
      },
    ],
    //组件状态
    SitePartsStaList: [{
      label: "废弃",
      value: "0",
      raw: {
        listClass: "info",
      },
    },
    {
      label: "在用",
      value: "1",
      raw: {
        listClass: "success",
      },
    },
    {
      label: "备用",
      value: "2",
      raw: {
        listClass: "primary",
      },
    },
    ],
    // 降雨量统计类型
    SiteRainfallStatisticsList:[
      {
        label: "1小时",
        value: "1",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "3小时",
        value: "9",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "6小时",
        value: "10",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "12小时",
        value: "2",
        raw: {
          listClass: "info",
        },
      },

    ],
    mfcgList:[
      {
        label: "建设单位",
        value: "1",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "承建单位",
        value: "2",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "运维单位",
        value: "3",
        raw: {
          listClass: "info",
        },
      },
    ],
    partsKindList:[
      {
        label: "翻斗雨量计",
        value: "109001",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "压力水位计",
        value: "109002",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "串口摄像头",
        value: "109003",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "声波水位计",
        value: "109004",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "格林水位计",
        value: "109005",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "联塑",
        value: "109006",
        raw: {
          listClass: "info",
        },
      },
    ],
    mtStaList:[
      {
        label: "在保",
        value: "1",
        raw: {
          listClass: "primary",
        },
      }, {
        label: "即将过保",
        value: "2",
        raw: {
          listClass: "warning",
        },
      },{
        label: "过保",
        value: "3",
        raw: {
          listClass: "danger",
        },
      }
    ],
    ptStaList:[
      {
        label: "在用",
        value: "1",
        raw: {
          listClass: "primary",
        },
      }, {
        label: "报废",
        value: "0",
        raw: {
          listClass: "danger",
        },
      }
    ],
    maintenanceStaList:[
      {
        label: "正常",
        value: "1",
        raw: {
          listClass: "primary",
        },
      }, {
        label: "告警",
        value: "2",
        raw: {
          listClass: "warning",
        },
      }, {
        label: "故障",
        value: "3",
        raw: {
          listClass: "danger",
        },
      }, {
        label: "维修",
        value: "4",
        raw: {
          listClass: "warning",
        },
      }
    ],

    ThreeFactorsOfSiteResultsList: [
      {
        label: "正常",
        value: "1",
        raw: {
          listClass: "primary",
        },
      }, {
        label: "不正常",
        value: "0",
        raw: {
          listClass: "danger",
        },
      }, {
        label: "无",
        value: "-1",
        raw: {
          listClass: "info",
        },
      }
    ],
    // 有无
    YesNoList: [
      {
        label: "有",
        value: "1",
        raw: {
          listClass: "primary",
        },
      }, {
        label: "无",
        value: "0",
        raw: {
          listClass: "warning",
        },
      }
    ],

    InspectionPlanStaList: [
      {
        label: "未计划",
        value: "-1",
        raw: {
          listClass: "info",
        },
      }, {
        label: "未巡检",
        value: "0",
        raw: {
          listClass: "danger",
        },
      }, {
        label: "已巡检",
        value: "1",
        raw: {
          listClass: "success",
        },
      }
    ],
    wfResultList: [
      {
        label: "已通过",
        value: "1",
        raw: {
          listClass: "success",
        },
      }, {
        label: "已驳回",
        value: "0",
        raw: {
          listClass: "danger",
        },
      }
    ],
    inspectionResultList: [
      {
        label: "正常",
        value: "1",
        raw: {
          listClass: "success",
        },
      }, {
        label: "异常",
        value: "2",
        raw: {
          listClass: "danger",
        },
      }
    ],
    inspectionStaList: [
      {
        label: "未执行",
        value: "0",
        raw: {
          listClass: "danger",
        },
      }, {
        label: "已执行",
        value: "1",
        raw: {
          listClass: "success",
        },
      }
    ],
    planStaList: [
      {
        label: "制单",
        value: "1",
        raw: {
          listClass: "info",
        },
      }, {
        label: "待确认",
        value: "2",
        raw: {
          listClass: "primary",
        },
      }, {
        label: "巡检中",
        value: "3",
        raw: {
          listClass: "danger",
        },
      }, {
        label: "待审核",
        value: "4",
        raw: {
          listClass: "warning",
        },
      }, {
        label: "已完成",
        value: "5",
        raw: {
          listClass: "success",
        },
      },
    ],
    hitchStaList: [
      {
        label: "制单",
        value: "0",
        raw: {
          listClass: "info",
        },
      }, {
        label: "待确认",
        value: "1",
        raw: {
          listClass: "primary",
        },
      }, {
        label: "已确认",
        value: "2",
        raw: {
          listClass: "primary",
        },
      }, {
        label: "已终止",
        value: "3",
        raw: {
          listClass: "danger",
        },
      }, {
        label: "维修中",
        value: "4",
        raw: {
          listClass: "warning",
        },
      }, {
        label: "待验证",
        value: "5",
        raw: {
          listClass: "primary",
        },
      }, {
        label: "待审核",
        value: "6",
        raw: {
          listClass: "primary",
        },
      }, {
        label: "已完成",
        value: "7",
        raw: {
          listClass: "success",
        },
      },
    ],
    sitedetailsList: [
      {
        label: '实时信息',
        index: 1
      },
      {
        label: '基本信息',
        index: 2
      },
      {
        label: '报文信息',
        index: 3
      },
      {
        label: '监测要素',
        index: 4
      },
      {
        label: '巡检列表',
        index: 5
      },
      {
        label: '故障申报',
        index: 9
      },
      {
        label: '维修养护',
        index: 6
      },
      {
        label: '异常警告',
        index: 7
      },
      {
        label: '组件列表',
        index: 8
      }
    ],
    // 运行状态
    runStaList: [{
      label: "注册",
      value: "1",
      raw: {
        listClass: "info",
      },
    },
    {
      label: "调试",
      value: "2",
      raw: {
        listClass: "warning",
      },
    },
    {
      label: "试运行",
      value: "3",
      raw: {
        listClass: "success",
      },
    },
    {
      label: "正式运行",
      value: "4",
      raw: {
        listClass: "danger",
      },
    },
    {
      label: "关闭",
      value: "5",
      raw: {
        listClass: "danger",
      },
    }
    ],
    //站点状态
    siteStaList: [{
      label: "离线",
      value: "1",
      raw: {
        listClass: "info",
      },
    },
    {
      label: "缺测",
      value: "2",
      raw: {
        listClass: "warning",
      },
    },
    {
      label: "电压低",
      value: "3",
      raw: {
        listClass: "success",
      },
    },
    {
      label: "信号弱",
      value: "4",
      raw: {
        listClass: "danger",
      },
    },
    {
      label: "停用",
      value: "5",
      raw: {
        listClass: "danger",
      },
    },
    {
      label: "正常",
      value: "6",
      raw: {
        listClass: "primary",
      },
    }
    ],
    //维保状态
    mtStaList: [{
      label: "在保",
      value: "1",
      raw: {
        listClass: "primary",
      },
    },
    {
      label: "即将过保",
      value: "2",
      raw: {
        listClass: "warning",
      },
    },
    {
      label: "已过保",
      value: "3",
      raw: {
        listClass: "danger",
      },
    }
    ],
    // 故障类型
    trblTypeList: [{
      label: "离线",
      value: "1",
      raw: {
        listClass: "info",
      },
    },
    {
      label: "缺测",
      value: "2",
      raw: {
        listClass: "warning",
      },
    },
    {
      label: "电压低",
      value: "3",
      raw: {
        listClass: "success",
      },
    },
    {
      label: "信号弱",
      value: "4",
      raw: {
        listClass: "danger",
      },
    },
    {
      label: "数据异常-其他",
      value: "5",
      raw: {
        listClass: "danger",
      },
    },
    {
      label: "数据异常-冒大数",
      value: "6",
      raw: {
        listClass: "danger",
      },
    },
    {
      label: "数据异常-冒小数",
      value: "7",
      raw: {
        listClass: "danger",
      },
    }
    ],

    // 处理状态
    handleList: [{
      label: "待确认",
      value: "1",
      raw: {
        listClass: "danger",
      },
    },
    {
      label: "待处理",
      value: "2",
      raw: {
        listClass: "warning",
      },
    },
    {
      label: "已处理",
      value: "3",
      raw: {
        listClass: "info",
      },
    },
      {
        label: "已忽略",
        value: "4",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "已恢复",
        value: "5",
        raw: {
          listClass: "info",
        },
      }
    ],
    // 监控状态
    monitorList: [{
      label: "监控中",
      value: "1",
      raw: {
        listClass: "success",
      },
    },
    {
      label: "未监控",
      value: "2",
      raw: {
        listClass: "warning",
      },
    }
    ],
    // 分组类型
    groupTypeList: [{
      label: "巡检组",
      value: "0",
      utype: '600000',
      raw: {
        listClass: "info",
      },
    },
    {
      label: "运维组",
      utype: '600001',
      value: "1",
      raw: {
        listClass: "info",
      },
    },
    {
      label: "维修组",
      utype: '600002',
      value: "2",
      raw: {
        listClass: "info",
      },
    },
    ],
    // 测站配件类型
    partsTypeList: [{
      label: "雨量计",
      value: "107000",
      raw: {
        listClass: "info",
      },
    },
    {
      label: "水位计",
      value: "107001",
      raw: {
        listClass: "info",
      },
    },
    {
      label: "摄像头",
      value: "107002",
      raw: {
        listClass: "info",
      },
    },
    {
      label: "遥测终端机",
      value: "107003",
      raw: {
        listClass: "info",
      },
    },
    {
      label: "物联网卡",
      value: "107004",
      raw: {
        listClass: "info",
      },
    },
    {
      label: "补光灯",
      value: "107005",
      raw: {
        listClass: "info",
      },
    },
    {
      label: "电池",
      value: "107006",
      raw: {
        listClass: "info",
      },
    },
    {
      label: "太阳能电源系统",
      value: "107007",
      raw: {
        listClass: "info",
      },
    },
    {
      label: "防雷接地",
      value: "107008",
      raw: {
        listClass: "info",
      },
    },
      {
        label: "充电控制器",
        value: "107009",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "室外防水箱",
        value: "107010",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "线缆及配件",
        value: "107011",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "立杆",
        value: "107012",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "设备箱",
        value: "107013",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "其他",
        value: "107014",
        raw: {
          listClass: "info",
        },
      }
    ],
    // 异常警告区间类型
    warnIntervalList: [{
      label: "大于",
      value: "1",
      raw: {
        listClass: "info",
      },
    },
    {
      label: "大于等于",
      value: "10",
      raw: {
        listClass: "info",
      },
    },
    {
      label: "小于",
      value: "2",
      raw: {
        listClass: "info",
      },
    },
    {
      label: "小于等于",
      value: "20",
      raw: {
        listClass: "info",
      },
    },

    {
      label: "等于",
      value: "3",
      raw: {
        listClass: "info",
      },
    }
    ],
    // 异常时间类型
    warnSTypeList: [
      {
        label: "1小时",
        value: "1",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "2小时",
        value: "2",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "6小时",
        value: "3",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "12小时",
        value: "4",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "24小时",
        value: "5",
        raw: {
          listClass: "info",
        },
      }
    ],
    // 异常阈值维度类型
    warnVTypeList: [
      {
        label: "当前值",
        value: "1",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "累积值",
        value: "2",
        raw: {
          listClass: "info",
        },
      },
      {
        label: "对比值",
        value: "3",
        raw: {
          listClass: "info",
        },
      }
    ],
    // 异常警告类型
    warnTypeList: [{
      label: "站点离线",
      value: "1",
      raw: {
        listClass: "info",
      },
    },
    {
      label: "站点缺测",
      value: "2",
      raw: {
        listClass: "info",
      },
    },
    {
      label: "电压异常",
      value: "3",
      raw: {
        listClass: "info",
      },
    },
    {
      label: "信号异常",
      value: "4",
      raw: {
        listClass: "info",
      },
    },
    {
      label: "图片异常",
      value: "5",
      raw: {
        listClass: "info",
      },
    },
    {
      label: "降雨量异常",
      value: "6",
      raw: {
        listClass: "info",
      },
    },
    {
      label: "水位异常",
      value: "7",
      raw: {
        listClass: "info",
      },
    }
    ],
    // 报文状态：1-正常入库 2-代表异常
    messageList: [{
      label: "正常",
      value: "1",
      raw: {
        listClass: "primary",
      },
    },
    {
      label: "异常",
      value: "0",
      raw: {
        listClass: "warning",
      },
    }
    ],
    //是否
    isPublicList: [{
      label: "是",
      value: "1",
      raw: {
        listClass: "primary",
      },
    },
    {
      label: "否",
      value: "0",
      raw: {
        listClass: "warning",
      },
    }
    ],
    //男女
    isGender: [{
      label: "男",
      value: "0",
      raw: {
        listClass: "primary",
      },
    },
    {
      label: "女",
      value: "1",
      raw: {
        listClass: "warning",
      },
    }
    ],
    //启用状态
    enableStaList: [{
      label: "未启用",
      value: "0",
      raw: {
        listClass: "warning",
      },
    }, {
      label: "启用",
      value: "1",
      raw: {
        listClass: "primary",
      },
    }
    ],
    // 操作结果
    resultTypeList: [{
      label: "失败",
      value: "0",
      raw: {
        listClass: "warning",
      },
    }, {
      label: "成功",
      value: "1",
      raw: {
        listClass: "primary",
      },
    }
    ],
    // 操作类型
    logOptTypeList: [{
      label: "默认",
      value: "default",
      raw: {
        listClass: "info",
      },
    }, {
      label: "登录",
      value: "login",
      raw: {
        listClass: "info",
      },
    }, {
      label: "登出",
      value: "logout",
      raw: {
        listClass: "info",
      },
    }, {
      label: "新增",
      value: "create",
      raw: {
        listClass: "info",
      },
    }, {
      label: "更新",
      value: "update",
      raw: {
        listClass: "info",
      },
    }, {
      label: "删除",
      value: "delete",
      raw: {
        listClass: "info",
      },
    }, {
      label: "导入",
      value: "import",
      raw: {
        listClass: "info",
      },
    }, {
      label: "导出",
      value: "export",
      raw: {
        listClass: "info",
      },
    }, {
      label: "绑定",
      value: "bind",
      raw: {
        listClass: "info",
      },
    }, {
      label: "取消绑定",
      value: "unbind",
      raw: {
        listClass: "info",
      },
    }, {
      label: "下发指令",
      value: "issue_ins",
      raw: {
        listClass: "info",
      },
    }
    ],
    // 人员结果
    positionTypeList: [{
      label: "巡检员",
      value: "巡检员",
      raw: {
        listClass: "info",
      },
    }, {
      label: "巡检负责人",
      value: "巡检负责人",
      raw: {
        listClass: "info",
      },
    }, {
      label: "运维人员",
      value: "运维人员",
      raw: {
        listClass: "info",
      },
    }, {
      label: "运维负责人",
      value: "运维负责人",
      raw: {
        listClass: "info",
      },
    }, {
      label: "维修员",
      value: "维修员",
      raw: {
        listClass: "info",
      },
    }, {
      label: "维修负责人",
      value: "维修负责人",
      raw: {
        listClass: "info",
      },
    }
    ],
    // 巡检人员
    positionTypeList2: [{
      label: "巡检员",
      value: "巡检员",
      raw: {
        listClass: "info",
      },
    }, {
      label: "巡检负责人",
      value: "巡检负责人",
      raw: {
        listClass: "info",
      },
    },
    ],
  },

  mutations: {

  },

  actions: {

  }
}

export default status
