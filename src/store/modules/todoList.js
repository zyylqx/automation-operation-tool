const state = {
    isOpen2:false,
    isNotifyDisplayed:false

}
const mutations = {
  SET_OPEN2:(state,value)=>{
    state.isOpen2=value
  },
  TOGGLE_OPEN2(state) {
    state.isOpen2 = !state.isOpen2;
  },
  SET_IsNotifyDisplayed:(state,value)=>{
    state.isNotifyDisplayed=value
  },
}

const actions = {
  setOpen2({ commit }, value) {
    commit('SET_OPEN2', value);
  },
  
  toggleOpen2({ commit }) {
    commit('TOGGLE_OPEN2');
  }
}

export default {
  namespaced: true,
  state,
  mutations
}

