import {
  login,
  logout,
  getInfo
} from '@/api/login'
import {
  getToken,
  setToken,
  removeToken
} from '@/utils/auth'

const user = {
  state: {
    token: getToken(),
    userId: '',
    name: '',
    avatar: '',
    phonenumber:'',
    company:'',
    companyName:'',
    roles: [],
    permissions: []
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_USERID: (state, userId) => {
      state.userId = userId
    },
    SET_NAME: (state, name) => {
      state.name = name
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_PHONENUMBER: (state, phonenumber) => {
      state.phonenumber = phonenumber
    },
    SET_COMPANY: (state, company) => {
      state.company = company
    },
    SET_COMPANYNAME: (state, companyName) => {
      state.companyName = companyName
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    },
    SET_PERMISSIONS: (state, permissions) => {
      state.permissions = permissions
    }
  },

  actions: {
    // 登录
    Login({
      commit
    }, userInfo) {
      const username = userInfo.username.trim()
      const password = userInfo.password
      const code = userInfo.code
      const uuid = userInfo.uuid
      return new Promise((resolve, reject) => {
        login(username, password, code, uuid).then(res => {
          setToken(res.result.access_token)
          commit('SET_TOKEN', res.result.access_token)
          resolve()
        }).catch(error => {
          console.log(error)
          reject(error)
        })
      })
    },

    // 获取用户信息
    GetInfo({
      commit,
      state
    }) {
      return new Promise((resolve, reject) => {
        getInfo().then(res => {
          const user = res.result
          const avatar = (user.avatar == "" || user.avatar == null) ? require("@/assets/images/profile.jpg") : user.avatar;
          if (res.result.roles && res.result.roles.length > 0) { // 验证返回的roles是否是一个非空数组
            commit('SET_ROLES', res.result.roleName)
            commit('SET_PERMISSIONS', res.result.permissions)
          } else {
            commit('SET_ROLES', ['ROLE_DEFAULT'])
          }
          // console.log(user.userId)
          commit('SET_USERID', user.userId)
          commit('SET_PERMISSIONS', user.permissionValueList)
          commit('SET_NAME', user.userName)
          commit('SET_AVATAR', avatar)
          commit('SET_PHONENUMBER', user.phoneNumber)
          commit('SET_COMPANY', user.company)
          commit('SET_COMPANYNAME', user.companyName)
          resolve(res)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 主动退出系统
    LogOut({
      commit,
      state
    }) {
      return new Promise((resolve, reject) => {
        logout(state.token).then(() => {
          commit('SET_TOKEN', '')
          commit('SET_ROLES', [])
          commit('SET_PERMISSIONS', [])
          removeToken()
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },
    // toke失效oa退出系统
    tokenLogOut({
      commit,
      state
    }) {
      return new Promise((resolve, reject) => {
        commit('SET_TOKEN', '')
        commit('SET_ROLES', [])
        commit('SET_PERMISSIONS', [])
        removeToken()
        resolve()
      })
    },
    // 前端 登出
    FedLogOut({
      commit
    }) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '')
        removeToken()
        resolve()
      })
    }
  }
}

export default user
